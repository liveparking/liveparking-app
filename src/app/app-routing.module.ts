import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingPreloaderService } from './services/app-routing-preloader.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./pages/details/details.module').then(m => m.DetailsPageModule),
    data: { preload: true }
  },
  {
    path: 'citylist',
    loadChildren: () => import('./pages/citylist/citylist.module').then(m => m.CitylistPageModule)
  },
  {
    path: 'more',
    loadChildren: () => import('./pages/more/more.module').then(m => m.MorePageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'feedback-form',
    loadChildren: () => import('./pages/feedback-form/feedback-form.module').then(m => m.FeedbackFormPageModule)
  },
  {
    path: 'list-view',
    loadChildren: () => import('./pages/list-view/list-view.module').then(m => m.ListViewPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./pages/faq/faq.module').then(m => m.FaqPageModule)
  },
  {
    path: 'buy-pro',
    loadChildren: () => import('./pages/buy/buy.module').then(m => m.BuyPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: AppRoutingPreloaderService })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
