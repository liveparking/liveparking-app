import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage-angular';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { slideTransition } from './transitions/page-transition';
import { environment } from 'src/environments/environment'

// Modals
import { UpsellModalComponent } from './components/modals/upsell-modal/upsell-modal.component';
import { CityNotAvailableComponent } from './components/modals/city-not-available/city-not-available.component';
import { OnboardingModalComponent } from './components/modals/onboarding-modal/onboarding-modal.component';
import { InteractionComponent } from './components/interaction/interaction.component';
import { LocationErrorModalComponent } from './components/location-error-modal/location-error-modal.component';

// Native
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Spherical, LocationService } from '@ionic-native/google-maps/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { LaunchReview } from '@ionic-native/launch-review/ngx';
import { Purchases } from '@ionic-native/purchases/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
// import { AdMob } from '@admob-plus/ionic';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Modules
import { OrderModule } from 'ngx-order-pipe';
import { DecimalPipe } from '@angular/common';

// Used in DetailsPage & AnalyticsPage
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import * as SolidGauge from 'highcharts/modules/solid-gauge.src';

// Lottie
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
export function playerFactory() {
  return player;
}

// Locale
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
registerLocaleData(localeDe, 'de');

import * as Sentry from '@sentry/angular';
Sentry.init({
  enabled: environment.production, // Only track in production
  dsn: 'https://b3035ec50a49404192da582e86e6650a@o453168.ingest.sentry.io/5441676',
  environment: environment.production ? 'production' : 'development',
  maxValueLength: 5000
});

@NgModule({
  declarations: [
    AppComponent,
    OnboardingModalComponent,
    CityNotAvailableComponent,
    UpsellModalComponent,
    InteractionComponent,
    LocationErrorModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartModule,
    OrderModule,
    FormsModule,
    BrowserAnimationsModule,
    LottieModule.forRoot({ player: playerFactory }),
    IonicStorageModule.forRoot(),
    IonicModule.forRoot({
      mode: 'ios',
      backButtonText: '⠀⠀⠀⠀',
      spinner: 'crescent',
      navAnimation: slideTransition
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    AppVersion,
    SafariViewController,
    OpenNativeSettings,
    DecimalPipe,
    Spherical,
    LocationService,
    Network,
    Dialogs,
    LaunchReview,
    Purchases,
    Keyboard,
    // AdMob,
    Appsflyer,
    InAppBrowser,
    { provide: LOCALE_ID, useValue: 'de' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HIGHCHARTS_MODULES, useFactory: () => [more, exporting, SolidGauge] }, // add as factory to your providers
    { provide: ErrorHandler, useValue: Sentry.createErrorHandler() }
  ],
  bootstrap: [AppComponent],
  exports: [ChartModule]
})

export class AppModule { }
