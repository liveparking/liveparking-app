import { Component, OnInit } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { LaunchReview } from '@ionic-native/launch-review/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { Keyboard, KeyboardResizeMode } from '@ionic-native/keyboard/ngx';
import { RestService } from './services/rest.service';
import { environment } from '../environments/environment';
import * as semver from 'semver';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private dialogs: Dialogs,
    private launchReview: LaunchReview,
    private safariViewController: SafariViewController,
    private appVersion: AppVersion,
    private network: Network,
    private restService: RestService,
    private toastController: ToastController,
    private appsFlyer: Appsflyer,
    private keyboard: Keyboard) { }

  async ngOnInit() {
    await this.initNativePlugins();
    setTimeout(async () => {
      await this.checkForAppUpdate();
    }, 2000);
  }

  async initNativePlugins() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      // Set resize mode here, because setting in config.xml isn't working
      this.keyboard.setResizeMode(KeyboardResizeMode.None);
      // this.checkNetwork();
      await this.initAppsFlyer();
    }
  }

  async initAppsFlyer() {
    try {
      await this.appsFlyer.initSdk({
        devKey: '9JHSaJcQ8akvHzPvuwLyCL',
        appId: '1454385584',
        isDebug: !environment.production,
        waitForATTUserAuthorization: 10,
      });
    } catch (e) {
      console.error(e);
    }
  }

  checkNetwork() {
    // watch network for a disconnection
    this.network.onDisconnect().subscribe(async () => {
      const toast = await this.toastController.create({
        message: 'Schlechte Internetverbindung',
        duration: 2000
      });
      toast.present();
    });
  }

  async checkForAppUpdate() {

    const appStoreVersion: any = await this.restService.getAppStoreVersion();
    let installedVersion: string;

    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const platform = this.platform.is('ios') ? 'ios' : 'android';
      try {
        installedVersion = await this.appVersion.getVersionNumber();

        if (semver.satisfies(installedVersion, '<' + appStoreVersion[platform].version)) {
          await this.showUpdateNotification();
        }
      } catch (e) {
        console.error(e);
      }
    }
  }

  async showUpdateNotification() {
    await this.platform.ready();
    if (this.platform.is('cordova')) { }
    const buttonIndex = await this.dialogs.confirm('Wir empfehlen Dir, immer die neueste verfügbare Version von LiveParking zu verwenden um von neuen Funktionen und Fehlerbehebungen zu profitieren.', 'Neue Version', ['Später', 'Jetzt laden']);
    if (buttonIndex === 1) {
      // dialog will be dismissed
    } else if (buttonIndex === 2) {
      if (this.platform.is('ios')) {
        // We can't use LaunchReview here because it'll open the review window in the App Store
        await this.safariViewController.show({ url: 'itms-apps://itunes.apple.com/app/apple-store/id1454385584?mt=8' }).toPromise();
      } else {
        // On Android it only opens the store page
        await this.launchReview.launch();
      }
    }
  }
}
