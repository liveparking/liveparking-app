import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private _storage: Storage | null = null;

  constructor(private storage: Storage) {}

  private async storageReady() {
    if(!this._storage) {
      const storage = await this.storage.create();
      this._storage = storage;
    }
  }

  /**
 * Set the value for the given key.
 * @param key the key to identify this value
 * @param value the value for this key
 * @returns Returns a promise that resolves when the key and value are set
 */
  public async set(key: string, value: any) {
    await this.storageReady();
    return this._storage?.set(key, value);
  }

  /**
 * Get the value associated with the given key.
 * @param key the key to identify this value
 * @returns Returns a promise with the value of the given key
 */
  public async get(key: string) {
    await this.storageReady();
    return this._storage?.get(key);
  }

  /**
   * Remove any value associated with this key.
   * @param key the key to identify this value
   * @returns Returns a promise that resolves when the value is removed
   */
  public async remove(key: string) {
    await this.storageReady();
    return this._storage?.remove(key);
  }

  /**
   * Clear the entire key value store. WARNING: HOT!
   * @returns Returns a promise that resolves when the store is cleared
   */
  public async clear() {
    await this.storageReady();
    return this._storage?.clear();
  }
}
