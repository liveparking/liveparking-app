import { Injectable, OnInit } from '@angular/core';
import { Purchases, PurchasesPackage, PurchasesOfferings, PURCHASE_TYPE, PurchasesError } from '@ionic-native/purchases/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { Platform, LoadingController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { RestService } from '../services/rest.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  private offerings: PurchasesOfferings = null;
  private loading: HTMLIonLoadingElement;
  private revenueCatReady = false;

  constructor(
    private purchases: Purchases,
    private dialogs: Dialogs,
    private platform: Platform,
    private storage: StorageService,
    private restService: RestService,
    private loadingController: LoadingController) { }

  async ngOnInit() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      // preload offerings
      console.log('Preloaded Offerings');
      await this.getOfferings();
    }
  }

  /**
   * IMPORTANT: Call this function every time before calling any other
   * revenue cat function to make sure everything is set up correctly.
   */
  private async preparePurchasesPlugin() {
    await this.platform.ready();
    if (this.platform.is('cordova') && !this.revenueCatReady) {
      this.revenueCatReady = true;
      this.purchases.setDebugLogsEnabled(!environment.production);
      this.purchases.setup(environment.revenueCatApiKey);
      console.log('Revenue is set up');
    }
  }

  private async showLoading() {
    this.loading = await this.loadingController.create({
      duration: 10000,
    });
    await this.loading.present();
  }

  private async hideLoading() {
    await this.loading.dismiss();
  }

  async getOfferings() {
    await this.preparePurchasesPlugin();
    if (this.platform.is('cordova') && !this.offerings) {
      this.offerings = await this.purchases.getOfferings();
    }
    console.log('Offerings' ,this.offerings);
    return this.offerings || null;
  }

  /**
   * Returns true if purchase was successful, returns false if purchase failed
   * @param purchasePackage 
   * @returns Result
   */
  async purchaseProduct(purchasePackage: PurchasesPackage): Promise<Result> {
    console.log('purchasePackage', purchasePackage);
    if (!purchasePackage) {
      return this.result(false);
    }
    await this.preparePurchasesPlugin();
    if (this.platform.is('cordova')) {
      try {
        await this.preparePurchasesPlugin();
        const { purchaserInfo } = await this.purchases.purchasePackage(purchasePackage);
        console.log('purchaserInfo', purchaserInfo);
        if (purchaserInfo.activeSubscriptions.length > 0) {
          await this.dialogs.alert('Du kannst nun alle Funktionen der PRO-Version nutzen.', 'Glückwunsch 🎉', 'Schließen');
          return this.result(true);
        } else {
          return this.result(false);
        }
      } catch (e) {
        if (!e?.userCancelled) {
          console.error(e);
          await this.dialogs.alert((e as PurchasesError)?.message, 'Fehler', 'Schließen');
        }
        return this.result(false);
      }
    }
  }

  async restorePurchase(): Promise<Result> {
    await this.preparePurchasesPlugin();
    if (this.platform.is('cordova')) {
      try {
        await this.showLoading();
        const resp = await this.purchases.restoreTransactions();
        if (resp.activeSubscriptions.length) {
          // await this.hideLoading();
          await this.dialogs.alert('Du kannst nun alle Funktionen der PRO-Version nutzen.', 'Glückwunsch 🎉', 'Schließen');
          return this.result(true);
        } else {
          // await this.hideLoading();
          await this.dialogs.alert('Es konnten keine aktiven Abonnements gefunden werden.', 'Keine aktiven Abos', 'Schließen');
          return this.result(false);
        }
      } catch (e) {
        // await this.hideLoading();
        await this.dialogs.alert((e as PurchasesError)?.message, 'Fehler', 'Schließen');
        return this.result(false);
      } finally {
        await this.hideLoading();
      }
    }
  }

  getPurchaserInfo() {
    return this.purchases.getPurchaserInfo();
  }

  async checkSubscriptionStatus() {
    await this.preparePurchasesPlugin();
    const couponIsActive = await this.checkForActiveCoupon();
    await this.purchases.getPurchaserInfo();
    const purchaserInfo = await this.purchases.getPurchaserInfo();
    console.log('purchaserInfo', purchaserInfo);
    if (!couponIsActive && !purchaserInfo?.activeSubscriptions?.length) {
      return false;
    } else {
      return true;
    }

  }

  async openCouponAlert() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const prompt = await this.dialogs.prompt('Hast Du einen Gutscheincode? Dann kannst Du ihn hier eingeben. Bereits aktive Gutscheincodes werden überschrieben.', 'Gutscheincode', ['Abbrechen', 'Einlösen']);
      if (prompt.buttonIndex === 2) {
        if (prompt.input1.length) {
          return await this.validateCouponCode(prompt.input1);
        }
      }
    }
  }

  private async validateCouponCode(code: string): Promise<Result> {
    await this.showLoading();
    const resp = await this.restService.verifyCouponCode(code);
    await this.hideLoading();
    // Check if coupon code is available
    if (resp.length) {
      // Check if coupon code is valid (also valid if 'validUntil' is null)
      if (Date.parse(resp[0].validUntil) >= Date.now() || resp[0].validUntil === null) {
        const redeemed = await this.checkIfCodeRedeemedBefore(resp[0].code);
        if (redeemed) {
          await this.dialogs.alert('Du kannst den selben Code nicht mehrmals einlösen :(', 'Code bereits eingelöst', 'Schließen');
          return { success: false }
        } else {
          await this.setCoupon(resp[0]);
          await this.dialogs.alert('Du kannst nun alle Funktionen der PRO-Version nutzen.', 'Glückwunsch 🎉', 'Schließen');
          return { success: true }
        }
      } else {
        await this.dialogs.alert('Der eingegebene Gutscheincode ist leider nicht (mehr) gültig.', 'Üngültiger Code', 'Schließen');
        return { success: false }
      }
    } else {
      await this.dialogs.alert('Der eingegebene Gutscheincode ist leider nicht (mehr) gültig.', 'Üngültiger Code', 'Schließen');
      return { success: false }
    }
  }

  /**
  * Check if user already applied this coupon code
  * @param couponCode
  */
  private async checkIfCodeRedeemedBefore(couponCode: string) {
    const redeemedCodes: string[] | null = await this.storage.get('redeemedCouponCodes');
    // If array isn't available, create it
    if (!redeemedCodes) {
      await this.storage.set('redeemedCouponCodes', []);
      return false; // Not redeemed
    } else {
      return redeemedCodes.includes(couponCode) ? true : false;
    }
  }

  private async addCouponToRedeemedCouponCodesList(couponCode: string) {
    let redeemedCodes: string[] = await this.storage.get('redeemedCouponCodes');
    if (!redeemedCodes) {
      redeemedCodes = [];
    }
    redeemedCodes.push(couponCode);
    console.log('Redeemed Coupon Codes', redeemedCodes);
    await this.storage.set('redeemedCouponCodes', redeemedCodes);
  }

  /**
   * Store response from backend
   * @param couponOptions
   */
  async setCoupon(couponOptions: CouponOptions) {
    console.log('Set Coupon', couponOptions);
    await this.addCouponToRedeemedCouponCodesList(couponOptions?.code);
    couponOptions['redeemed_at'] = Date.now();
    couponOptions['expire_at'] = Date.now() + 1000 * 60 * 60 * 24 * couponOptions.trialDays;
    await this.storage.set('activeCoupon', couponOptions);
  }

  /**
   * Get metadata of the currently applied coupon
   */
  getActiveCoupon(): Promise<CouponOptions> {
    return this.storage.get('activeCoupon');
  }

  /**
   * Check if user applied an coupon code
   * true = yes, false = no
   */
  async checkForActiveCoupon() {
    const coupon: CouponOptions = await this.storage.get('activeCoupon');
    if (coupon) {
      if (!coupon.trialDays || (Date.now() < coupon.expire_at)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  private result(success: boolean): Result {
    return {
      success
    }
  }

}


interface Result {
  success: boolean;
  data?: any;
}

export interface CouponOptions {
  id: number;
  code: string;
  validUntil: number;
  trialDays: number;
  created_at: string;
  updated_at: string;
  redeemed_at?: number; // Time in Millis
  expire_at?: number; // Time in Millis
}
