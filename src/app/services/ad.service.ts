import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
// import { AdMob } from '@admob-plus/ionic';

@Injectable({
  providedIn: 'root'
})
export class AdService {

  constructor(
    private platform: Platform) { }

  async showBanner() {
    try {
      await this.platform.ready();
      if (this.platform.is('cordova')) {
        // this.adMob.setDevMode(!environment.production);
        // await this.adMob.banner.show({
        //   id: {
        //     ios: environment.adMobBannerIdIOS,
        //     android: environment.adMobBannerIdAndroid,
        //   },
        //   size: {
        //     width: window.innerWidth,
        //     height: 50
        //   }
        // });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async removeBanner() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      // await this.adMob.banner.hide({
      //   ios: environment.adMobBannerIdIOS,
      //   android: environment.adMobBannerIdAndroid,
      // });
    }
  }
}
