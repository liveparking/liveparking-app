import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscriber } from 'rxjs';
import { StorageService } from '../services/storage.service';
import { Platform } from '@ionic/angular';
import { interval, Subscription, Observable } from 'rxjs';
import { ParkingOptions } from './rest.service';
import { defaultMapStyle } from '../../assets/map/default';
import { GoogleMaps, GoogleMap, Spherical, LocationService, ILatLng, GoogleMapsEvent, Marker, CameraPosition, BaseArrayClass, MyLocation, GoogleMapOptions, Polyline, Polygon, Environment, Geocoder } from '@ionic-native/google-maps/ngx';

@Injectable({
  providedIn: 'root'
})

export class MapService {

  map: GoogleMap;
  errorPopupShown = false;
  previousPosition: ILatLng;
  polylines: Polyline[] = [];
  existingItems = [];
  existingMarkers = [];
  markedPositions = [];
  private selectedMarker: BehaviorSubject<ParkingOptions>;

  constructor(
    private platform: Platform,
    private storage: StorageService) {
    this.selectedMarker = new BehaviorSubject(null);
  }

  async initMap() {
    const options: GoogleMapOptions = {
      styles: defaultMapStyle,
      preferences: {
        padding: {
          top: 40,
        },
        building: false,
        zoom: {
          minZoom: 15,
          maxZoom: 25
        },
      },
      controls: {
        compass: false,
        myLocation: true,
        myLocationButton: false,
      },
      camera: {
        target: {
          lat: 51.165691,
          lng: 10.451526
        }, // Middle of germany
        zoom: 18,
      }
    };

    await this.platform.ready();

    if (this.platform.is('cordova')) {
      this.map = await this.createMapEl(options);

      // Check if user has defined mapType from last session
      const mapType = await this.storage.get('mapType');
      await this.setMapType(mapType);
    }

  }

  mapReady() {
    return this.map.addEventListenerOnce('MAP_READY');
  }

  createMapEl(options: GoogleMapOptions): Promise<GoogleMap> {
    return new Promise(resolve => {
      const map = GoogleMaps.create('map_canvas', options);
      resolve(map);
    });
  }

  /**
   * Remove all overlays, such as marker
   */
  async clearMap() {
    this.existingItems = [];
    this.existingMarkers = [];
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.map.clear();
    }
  }

  /**
   * Remove all PolyLines from the map
   */
  async clearPolylines() {
    await Promise.all(this.polylines.map(async polyline => {
      return new Promise(resolve => {
        const result = polyline.remove();
        resolve(result);
      });
    }));
    // Clear existing items, otherwise previous object won't display again
    this.existingItems = [];
  }

  /**
   * Call this on pages with modals
   */
  setBackgroundToBlack() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        Environment.setBackgroundColor('black');
      }
    });
  }

  async setMarkers(rest: ParkingOptions[]) {
    if (this.platform.is('cordova')) {
      // Put the markers on the map
      const results: BaseArrayClass<ParkingOptions> = new BaseArrayClass<ParkingOptions>(rest);
      results.map(async data => {

        // Track id's, so there won't be double entries
        if (!this.existingMarkers.includes(data.id)) {
          this.existingMarkers.push(data.id);
        } else {
          return;
        }

        let text: any, hex: string;
        if (this.isNumber(data.freeSpots)) {
          text = data.freeSpots;
          hex = data.indicatorColor.replace('#', '')
        } else {
          text = 'P';
          hex = '4F536A';
        }

        const marker: Marker = await this.map.addMarker({
          position: data.location,
          icon: {
            url: `https://liveparking.eu/static/marker.png?text=${text}&hex=${hex}`,
            size: {
              width: 40,
              height: 40
            }
          }
        });
        marker.addEventListener(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.selectedMarker.next(data);
        });
      });
    }
  }

  markerClicked() {
    return this.selectedMarker.asObservable();
  }

  async selectMarker(location: ILatLng) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.map.animateCamera({
        target: location,
        zoom: 18,
        duration: 500
      });
    }
  }

  async addElementsToMap(itemx: ParkingOptions[]) {

    await this.platform.ready();

    if (this.platform.is('cordova')) {

      const items: BaseArrayClass<ParkingOptions> = new BaseArrayClass<ParkingOptions>(itemx);

      items.map(async item => {

        // Track id's, so there won't be double entries
        if (!this.existingItems.includes(item.id)) {
          this.existingItems.push(item.id);
        } else {
          return;
        }

        const points = [];

        if (item.geometry.type === 'LineString') {
          const coords: BaseArrayClass<any> = new BaseArrayClass(item.geometry.coordinates);
          coords.map((point: number) => {
            points.push({
              lat: point[1],
              lng: point[0]
            });
          });
          const polyline: Polyline = await this.map.addPolyline({
            points,
            width: 7,
            color: item.indicatorColor
          });
          polyline.setClickable(true);
          polyline.addEventListener(GoogleMapsEvent.POLYLINE_CLICK).subscribe(() => {
            this.selectedMarker.next(item);
          });
          this.polylines.push(polyline);
        }

        if (item.geometry.type === 'Polygon') {
          item.geometry.coordinates[0].map((point: number[]) => {
            points.push({
              lat: point[1],
              lng: point[0]
            });
          });
          const polygon: Polygon = await this.map.addPolygon({
            points,
            strokeWidth: 1,
            strokeColor: '#000000',
            fillColor: item.indicatorColor
          });
          polygon.setClickable(true);
          polygon.addEventListener(GoogleMapsEvent.POLYGON_CLICK).subscribe(() => {
            this.selectedMarker.next(item);
          });
        }

        if (item.geometry.type === 'Point') {
          const coords: any[] = item.geometry.coordinates;

          try {
            let text: any, hex: string;
            if (this.isNumber(item.freeSpots)) {
              text = item.freeSpots;
              hex = item.indicatorColor.replace('#', '');
            } else if (this.isNumber(item.parkingProbability)) {
              text = item.parkingProbability + encodeURIComponent('%');
              hex = item.indicatorColor.replace('#', '');
            } else {
              text = 'P';
              hex = '4F536A';
            }

            // Uncomment for demo data
            // text = this.randomIntFromInterval(10, 300);
            // hex = this.colorIndicatorByOccupancy(text).replace('#', '');

            const marker: Marker = await this.map.addMarker({
              position: {
                lat: coords[1],
                lng: coords[0],
              },
              icon: {
                url: `https://liveparking.eu/static/marker.png?text=${text}&hex=${hex}`,
                size: {
                  width: 40,
                  height: 40
                }
              }
            });
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
              this.selectedMarker.next(item);
            });
          } catch (error) {
            console.error(error);
          }
        }
      });
    }
  }

  async setMapType(mapType: 'MAP_TYPE_SATELLITE' | 'MAP_TYPE_NORMAL'): Promise<void> {
    await this.storage.set('mapType', mapType);
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await new Promise(resolve => {
        const options = this.map.setOptions({
          mapType
        });
        resolve(options);
      });
    }
  }

  async setTrafficEnabled(enabled: true | false): Promise<void> {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await new Promise(resolve => {
        const func = this.map.setTrafficEnabled(enabled);
        resolve(func);
      });
    }
  }

  /** Stored MapType Setting set by User */
  async getMapType(): Promise<'MAP_TYPE_SATELLITE' | 'MAP_TYPE_NORMAL'> {
    const mapType = await this.storage.get('mapType');
    return (mapType == null) ? 'MAP_TYPE_NORMAL' : mapType;
  }

  onMapDragEnd() {
    return this.map.addEventListener(GoogleMapsEvent.MAP_DRAG_END);
  }

  onCameraMoveEnd() {
    return this.map.addEventListener(GoogleMapsEvent.CAMERA_MOVE_END);
  }

  /**
   * Displays user's location on the map and also returns it as an object
   */
  async showUserLocation() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const location: MyLocation = await this.map.getMyLocation();
      const position: CameraPosition<any> = {
        target: location.latLng,
        bearing: location.bearing,
        zoom: 18,
        duration: 500
      };
      await this.map.animateCamera(position);
      return location.latLng;
    }
  }

  async getUserLatLng() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const location = await LocationService.getMyLocation();
      return location.latLng;
    } else {
      // Location of Cologne
      const location: ILatLng = {
        lat: 52.520008,
        lng: 13.404954
      };
      return location;
    }
  }

  /**
   * Returns the distance in meters
   * @param destination ILatLng
   */
  async calcDistance(destination: ILatLng = null): Promise<number> {

    // Get current location of the user/device
    if (destination) {
      await this.platform.ready();
      if (this.platform.is('cordova')) {
        const location = await LocationService.getMyLocation();
        return Spherical.computeDistanceBetween(location.latLng, destination);
      } else {
        // Just for testing on browser
        // tslint:disable-next-line: no-bitwise
        return ~~(Math.random() * 5000) + 1000;
      }

    }
  }

  /**
   * Returns the distance between to positions in meters
   * @param startPosition ILatLng
   * @param endPosition ILatLng
   */
  async calcDistanceBetweenPoints(startPosition: ILatLng, endPosition: ILatLng): Promise<number> {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      return Spherical.computeDistanceBetween(startPosition, endPosition);
    } else {
      // Just for testing on browser
      // tslint:disable-next-line: no-bitwise
      return ~~(Math.random() * 5000) + 1000;
    }
  }

  moveCameraToPosition(position: ILatLng) {
    return this.map.moveCamera({
      target: position,
      zoom: 17
    });
  }

  getCameraPosition() {
    return this.map.getCameraTarget();
  }

  addressToLatLng(address: string) {
    if (this.platform.is('cordova')) {
      return Geocoder.geocode({
        address
      });
    }
  }

  latLngToAddress(position: ILatLng) {
    if (this.platform.is('cordova')) {
      return Geocoder.geocode({
        position
      });
    }
  }

  async getCityMetaByMapPosition() {
    const mapPosition = this.getCameraPosition();
    const geocoding = await this.latLngToAddress(mapPosition);
    return geocoding[0];
  }

  animateCamera(target: ILatLng, zoom: number = 18, duration: number = 500) {
    return this.map.animateCamera({
      target,
      zoom,
      duration: 500
    });
  }

  async markPosition(key: any, position: ILatLng) {
    // this.markedPositions.push(key);
    // const i = this.markedPositions.findIndex(key);
    // this.markedPositions[i] = await this.map.addMarker({ position });
    await this.map.addMarker({
      position,
      icon: {
        url: 'https://liveparking.eu/static/location.png',
        size: {
          width: 50,
          height: 50
        }
      }
    });
  }

  removeMarkerByKey(key: any) {
    const i = this.markedPositions.findIndex(key);
    const marker: Marker = this.markedPositions[i];
    marker.remove();
  }

  /**
   * Set false to ignore all clicks on the map
   * @param isClickable {boolean}
   */
  setClickable(isClickable: boolean) {
    this.map.setClickable(isClickable);
  }

  bliqColorNameToHex(colorName: string) {
    switch (colorName) {
      case 'GREEN':
        return '#70bf3d';
      case 'GREEN_YELLOW':
        return '#9cb400';
      case 'YELLOW':
        return '#fdd800';
      case 'ORANGE':
        return '#f25900';
      case 'RED':
        return '#b70000';
      default:
        return '#0080ff';
    }
  }

  /**
   * Delivers new coordinates if position has changed by X meters
   */
  watchPosition(differenceInMeters: number): Observable<ILatLng> {
    return new Observable(subscriber => {
      interval(2000).subscribe(async () => {
        if (this.previousPosition) {
          const distance = await this.calcDistance(this.previousPosition);
          if (distance >= differenceInMeters) {
            const latLng = await this.getUserLatLng();
            this.previousPosition = latLng;
            subscriber.next(latLng);
          }
        } else {
          this.previousPosition = await this.getUserLatLng();
        }
      });
    });
  }

  private isNumber(n: any) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0)
  }

  private randomIntFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private colorIndicatorByOccupancy(freeSpots: number) {
    if (freeSpots > 50) {
      return '#70bf3d';
    } else if (freeSpots <= 50 && freeSpots > 20) {
      return '#9cb400';
    } else if (freeSpots <= 20 && freeSpots > 10) {
      return '#fdd800';
    } else if (freeSpots < 10 && freeSpots > 0) {
      return '#f25900';
    } else if (freeSpots === 0) {
      return '#b70000';
    } else {
      return '#5f5f5f';
    }
  }

  // Add a marker to the map an display fab button
  // if someone saved his parking position.
  /* initParkingPos() {
    this.storage.get('parkingPos').then((value) => {
      if (value) {
        this.settingsService.parkingPos = value;
        this.platform.ready().then(() => {
          if (this.platform.is('cordova')) {
            this.map.addMarker({
              'position': value,
              'icon': {
                'url': 'https://static.liveparking.eu/markers/v2/_car-position.png',
                'size': {
                  'width': 45,
                  'height': 26
                }
              }
            }).then((marker: Marker) => {
              marker.on(GoogleMapsEvent.MARKER_CLICK)
                .subscribe((data) => {
                  this.showCarPositionManager();
                });
            });
          }
        });
      }
    });
  } */

  /* async navigateToStoredPosition() {
    const position = await this.storage.get('parkingPos');
    if (position) {
      await this.platform.ready();
      if (this.platform.is('cordova')) {
        this.map.animateCamera({
          target: position,
          zoom: 18,
          duration: 500
        });
        setTimeout(() => {
          this.map.panBy(0, -100);
        }, 600);
      }
    }
  } */

  /* openNavigator() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {

        const options: LaunchNavigatorOptions = {
          appSelection: {
            dialogHeaderText: 'Zu ' + this.rest[this.current].title + ' navigieren:',
            cancelButtonText: 'Abbrechen',
            rememberChoice: {
              enabled: false
            }
          }
        };

        this.launchNavigator.navigate([this.rest[this.current].location.lat, this.rest[this.current].location.lng], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
      }
    });
  } */

}
export { ILatLng } from '@ionic-native/google-maps/ngx';
