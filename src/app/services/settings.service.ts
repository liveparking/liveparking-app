import { Injectable } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { ToastController, AlertController } from '@ionic/angular';
import { PurchasesPackage } from '@ionic-native/purchases/ngx';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private favorites = [];
  private offerings: PurchasesPackage[];
  private _trafficEnabled = new Subject<any>();

  constructor(
    private storage: StorageService,
    private alertController: AlertController,
    private toastController: ToastController) { }

  /** Reset all Settings */
  async reset() {
    await this.storage.clear();
  }

  /** Check if it's the first run of the app */
  async isFirstLaunch(): Promise<boolean> {
    const isFirstLaunch = await this.storage.get('isFirstLaunch');
    if (isFirstLaunch === null) {
      return true;
    } else {
      return false;
    }
  }

  /** ONBOARDING */

  async onboardingCompleted(value?: boolean): Promise<boolean> {
    if (value === true || value === false) {
      await this.storage.set('onboardingCompleted', value);
      return value;
    } else {
      return this.storage.get('onboardingCompleted');
    }
  }

  /** MAP OPTIONS */

  async offStreetParkingEnabled(): Promise<boolean> {
    const offStreetParkingEnabled = await this.storage.get('offStreetParkingEnabled');
    // Enabled by default
    if (offStreetParkingEnabled === null) {
      await this.setOffStreetParkingEnabled(true);
      return true;
    }
    return offStreetParkingEnabled;
  }

  setOffStreetParkingEnabled(value: boolean) {
    return this.storage.set('offStreetParkingEnabled', value);
  }

  onStreetParkingEnabled(): Promise<boolean> {
    return this.storage.get('onStreetParkingEnabled');
  }

  setOnStreetParkingEnabled(value: boolean) {
    return this.storage.set('onStreetParkingEnabled', value);
  }

  trafficEnabled(): Promise<boolean> {
    return this.storage.get('mapTrafficEnabled');
  }

  setTrafficEnabled(value: boolean) {
    this._trafficEnabled.next(value);
    return this.storage.set('mapTrafficEnabled', value);
  }

  trafficEnabledListener(): Observable<boolean> {
    return this._trafficEnabled.asObservable();
  }

  /** USER STATUS */

  isProUser(): Promise<boolean | undefined> {
    return this.storage.get('isProUser');
    // return new Promise(resolve => resolve(false)); // For testing
  }

  async setUserAsPro(value: boolean) {
    console.log('User is now PRO', value);
    await this.storage.set('isProUser', value);
  }


  setActiveSubscriptions(activeSubscriptions: string[]): Promise<void> {
    return this.storage.set('activeSubscriptions', activeSubscriptions);
  }

  getActiveSubscriptions(): Promise<string[]> {
    return this.storage.get('activeSubscriptions');
  }

  async removeProStatus() {
    await this.setUserAsPro(false);
    await this.storage.set('activeCoupon', null);
    await this.setActiveSubscriptions(null);
    await this.setOnStreetParkingEnabled(false);
  }
  /** USER DATA */

  getUserName() {
    return this.storage.get('userName');
  }

  async setUserName(fullName: string) {
    await this.storage.set('userName', fullName);
  }

  getUserEmail() {
    return this.storage.get('userEmail');
  }

  async setUserEmail(emailAddress: string) {
    await this.storage.set('userEmail', emailAddress);
  }

  /* CITY SELECTION (Basic User) */

  async setCity(city: CityInfo) {
    await this.storage.set('selectedCity', city);
  }

  async getCity(): Promise<CityInfo> {
    return await this.storage.get('selectedCity');
  }

  /** FAVORITES */

  async initFavorites() {
    const favorites = await this.storage.get('favorites');
    if (favorites) {
      this.favorites = favorites;
    }
  }

  getFavorites() {
    return this.favorites;
  }

  async addToFavorites(id: number) {
    if (this.isFavorite(id)) {
      await this.removeFromFavorites(id);
    } else {
      this.favorites.push(id);
      await this.storage.set('favorites', this.favorites);
      await this.presentToast('Als Favorit markiert.');
    }
  }

  private async removeFromFavorites(id: number) {
    this.favorites.forEach((item, index) => {
      if (item === id) {
        this.favorites.splice(index, 1);
      }
    });
    await this.storage.set('favorites', this.favorites);
    await this.presentToast('Aus Favoriten entfernt.');
  }

  isFavorite(id: number) {
    let isAvailable = false;
    this.favorites.forEach(item => {
      if (item === id) {
        isAvailable = true;
      }
    });
    return isAvailable;
  }

  async removeFavorite(id: number) {
    const alert = await this.alertController.create({
      header: 'Favorit entfernen?',
      message: 'Soll diese Parkmöglichkeit aus Deinen Favoriten entfernt werden?',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel'
        }, {
          text: 'Entfernen',
          handler: () => {
            // TODO: Add StorageService Function
          }
        }
      ]
    });

    await alert.present();
  }

  /** Car Finder */
  async savePosition() {
    const alert = await this.alertController.create({
      header: 'Position Merken?',
      message: 'Diese Funktion hilft Dir dabei, Dein Auto ganz einfach wieder finden zu können. <strong>Deine aktuelle Position</strong> wird hierzu auf der Karte markiert.',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Merken',
          handler: async () => {
            // TODO: Save Position in Storage
          }
        }
      ]
    });
    await alert.present();
  }

  /* async deleteCarPosition() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.map.clear();
      await this.setMarkers();
    }
  } */

  /* async showCarPositionManager() {
   const modal = await this.modalController.create({
     component: InteractionComponent,
     backdropDismiss: true,
     showBackdrop: false,
     id: 'interaction-element'
   });
   return await modal.present();
 } */

  /**
   * TOAST
   */

  private async presentToast(text: string) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000,
      color: 'dark'
    });
    toast.present();
  }

  // HELPERS

  /**
   * Store offerings locally to reduce load time
   */
  storeOfferingsForSession(offerings: PurchasesPackage[]) {
    this.offerings = offerings;
  }

  /**
   * Get offerings that are stored locally to reduce load time
   */
  getOfferingsFromSessionStorage(): PurchasesPackage[] {
    return this.offerings;
  }

  clearStorage() {
    return this.storage.clear();
  }

}

/**
 * INTERFACES
 */
interface CityInfo {
  id?: number;
  name: string;
  slug: string;
  country?: string;
  status?: string;
  location: {
    lat: string;
    lng: string;
  };
  credits?: any[];
}