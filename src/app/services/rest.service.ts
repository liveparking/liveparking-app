import { Injectable } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILatLng } from '@ionic-native/google-maps/ngx';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RestService {

  private legacyApiUrl = 'https://legacy-api.liveparking.eu/v2';
  private apiUrl = 'https://api.liveparking.eu';
  private bliqUrlV3 = 'https://api.bliq.ai/park/v3';
  private bliqUrlV1 = 'https://api.bliq.ai:443/park/v1';
  private apiKey = '540854ec-07b3-44b0-9774-9622b09682be';
  private bliqApiKey = 'vKO7vxxe9DarZ9FALZ1lZ8ATshPK7F4ceGbw30A6';
  private googleApiKey = 'AIzaSyA_1aWJNlsa0zVzlJxfMUvZCxopyYqcYc8'; // liveparking-projekt
  private navitiaApiKey = '77a7eca6-417e-4bc6-bc0c-11ce81854e90';
  private openChargeMapApiKey = '0831f46b-3fb2-4206-9b3a-cd80b4eaa522';

  constructor(private http: HttpClient, private alertController: AlertController, private platform: Platform) {
    this.apiUrl = environment.apiUrl;
  }

  /**
   * BLIQ (LiveParking Pro)
   */

  async getOnStreetParking(lat: number, lng: number) {
    try {
      const response: any = await this.http.post(`${this.bliqUrlV3}/getOnStreetParkingOptions`, {
        "localeDefinition": {
          "contentLanguage": "DE"
        },
        "mapLayers": [
          "RULES",
          "PREDICTION",
          "LIVE"
        ],
        "requestType": {
          "dataType": "NextToPointRequest",
          "value": {
            "point": {
              "type": "Point",
              "coordinates": [
                lng,
                lat
              ]
            },
            "limit": 20
          }
        }
      },
        {
          "headers": {
            "Content-Type": "application/json",
            "X-API-KEY": this.bliqApiKey
          }
        }).toPromise();
      // const response: any = await this.http.get('/assets/json/bliq-on-street-v3-sample.json').toPromise();
      const output: ParkingOptions[] = [];
      response.features.map((item: any) => {
        output.push({
          id: item.id,
          name: item.properties.parkingEntity?.streetName,
          type: 'On-Street',
          freeSpots: item.properties.parkingEntity?.occupancy?.occupancyLive?.minimumOpenSpots,
          parkingProbability: item.properties.parkingEntity?.occupancy?.occupancyPredicted?.parkingProbability,
          parkingProbabilityText: this.bliqColorProbabilityText(item.properties.parkingEntity.occupancy?.color),
          allSpots: item.properties.parkingEntity?.capacity,
          indicatorColor: this.bliqColorNameToHex(item.properties.parkingEntity.occupancy?.color),
          pricing: item.properties.parkingEntity.priceInformation?.stringRepresentation,
          // pricingShort: item.properties.parkingEntity.priceInformation?.simplePriceStatus,
          pricingShort: item.properties.parkingEntity.priceInformation?.stringRepresentation.split('\n').slice(-1)[0].replace(/ /g, '').replace('Immer', ''), // Split by new line, get last element of array, remove spaces. Also remove "Immer" from "Immer kostenlos",
          openingTimes: item.properties.parkingEntity.openingHours?.stringRepresentation,
          paymentMethods: 'Münzen, evtl. andere Zahlungsmittel',
          address: item.properties.parkingEntity.streetName,
          state: item.properties.parkingEntity.openingHours.simpleOpeningStatus,
          maxParkingTime: item.properties.parkingEntity?.maxStay?.length ? item.properties.parkingEntity.maxStay[0].allowedTimeInMinutes : null,
          facilityType: 'Parkstreifen',
          location: {
            lat: item.properties.parkingEntity.centerPoint.coordinates[1],
            lng: item.properties.parkingEntity.centerPoint.coordinates[0]
          },
          geometry: item.geometry
        });
      });
      return output;
    } catch {
      return [];
    }
  }

  async getOffStreetParking(lat: number, lng: number) {
    try {
      const response: any = await this.http.post(`${this.bliqUrlV3}/getOffStreetParkingOptions`, {
        "localeDefinition": {
          "contentLanguage": "DE"
        },
        "mapLayers": [
          "RULES",
          "PREDICTION",
          "LIVE"
        ],
        "requestType": {
          "dataType": "NextToPointRequest",
          "value": {
            "point": {
              "type": "Point",
              "coordinates": [
                lng,
                lat
              ]
            },
            "limit": 20
          }
        }
      },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-API-KEY': this.bliqApiKey
          }
        }).toPromise();
      // const response: any = await this.http.get('/assets/json/bliq-off-street-v3-sample.json').toPromise();
      const output: ParkingOptions[] = [];
      response.features.map((item: any) => {
        output.push({
          id: item.id,
          name: item.properties.parkingEntity?.name,
          type: 'Off-Street',
          freeSpots: item.properties.parkingEntity?.occupancy?.occupancyPredicted?.openSpots || item.properties.parkingEntity?.occupancy?.occupancyLive?.minimumOpenSpots,
          parkingProbability: item.properties.parkingEntity?.occupancy?.occupancyPredicted?.parkingProbability,
          parkingProbabilityText: this.bliqColorProbabilityText(item.properties.parkingEntity.occupancy?.color),
          allSpots: item.properties.parkingEntity?.capacity,
          indicatorColor: this.bliqColorNameToHex(item.properties.parkingEntity.occupancy?.color),
          handicappedSpots: item.properties.parkingEntity?.capacityHandicapped,
          pricing: item.properties.parkingEntity.priceInformation?.stringRepresentation.replace(/\n   /g, ' ').replace(/\n/g, '<br>').replace('immer', ''),
          pricingShort: item.properties.parkingEntity?.priceInformation?.schedules ? item.properties.parkingEntity?.priceInformation?.schedules[0]?.priceModel?.prices[0]?.priceValue + '€/Std.' : null,
          openingTimes: item.properties.parkingEntity.openingHours?.stringRepresentation,
          state: item.properties.parkingEntity.openingHours.simpleOpeningStatus,
          maxParkingTime: item.properties.parkingEntity?.maxStay?.length ? item.properties.parkingEntity.maxStay[0].allowedTimeInMinutes : null,
          maxHeight: item.properties.parkingEntity?.entrancePoints?.length ? item.properties.parkingEntity.entrancePoints[0]?.entranceHeight : null,
          location: {
            lat: item.properties.parkingEntity.centerPoint.coordinates[1],
            lng: item.properties.parkingEntity.centerPoint.coordinates[0]
          },
          geometry: item.geometry
        });
      });
      return output;
    } catch {
      return [];
    }
  }

  public askParkingAssistant(lat: number, lng: number) {
    return this.http.post(`${this.bliqUrlV3}/askParkingAssistant`, {
      localeDefinition: {
        contentLanguage: 'DE',
        currencyCode: 'EUR',
        unitSystem: 'METRIC'
      },
      outputFormat: 'GEO_JSON',
      mapLayers: [
        'RULES',
        'LIVE'
      ],
      requestType: {
        dataType: 'LastMileRoutingRequest',
        value: {
          departure: {
            type: 'Point',
            coordinates: [
              lng,
              lat
            ]
          },
          destination: {
            type: 'Point',
            coordinates: [
              lng,
              lat
            ]
          }
        }
      }
    },
      {
        headers: {
          'Content-Type': 'application/json',
          apikey: this.bliqApiKey
        }
      }).toPromise();
  }

  /**
   * Get parking options from LiveParking backend by coordinates
   * @param lat latitude
   * @param lng longitude
   * @param limit amount of results
   * @param distance distance in meters
   */
  public async getOnStreetParkingBasic(lat: number, lng: number, limit = 50, distance = 1000) {
    try {
      const response: any = await this.http.get(`${this.apiUrl}/facilities?lat=${lat}&lng=${lng}&limit=${limit}&distance=${distance}`).toPromise();
      const data: ParkingOptions[] = [];
      for (const item of response) {
        data.push({
          id: item.id,
          name: item.name,
          type: 'Off-Street',
          freeSpots: item.spots.free,
          freeSpotsText: item.spots.freeText,
          allSpots: item.spots.capacity,
          // tslint:disable-next-line: max-line-length
          indicatorColor: this.isNumber(item.spots.free) ? this.colorIndicatorByOccupancy(Number(item.spots.free)) : (item.spots.freeText === 'Frei' ? this.colorIndicatorByOccupancy(1000) : (item.spots.freeText === 'Besetzt' ? this.colorIndicatorByOccupancy(0) : this.colorIndicatorByOccupancy(null))),
          parkingProbabilityText: this.probabilityTextByOccupancy(item.spots.free),
          // handicappedSpots: item.disabled_parking,
          pricing: item?.pricing?.stringRepresentation,
          openingTimes: item?.openingTimes,
          state: item?.state,
          // paymentMethods: this.arrayToString(item?.payment),
          features: item.features,
          address: item?.address?.street ? item?.address?.street : null,
          extras: item?.notice,
          maxHeight: item?.entranceHeight,
          location: item.location
        });
      }
      return data;
    } catch {
      return [];
    }
  }

  public async getFacility(id: any): Promise<any> {
    return this.http.get(`${this.legacyApiUrl}/facility/${id}/?key=${this.apiKey}`).toPromise();
  }

  public async getCityIndex(): Promise<object> {
    return this.http.get(`${this.apiUrl}/cities`).toPromise();
  }

  public async getAppStoreVersion(): Promise<object> {
    return this.http.get(`${this.apiUrl}/app-info`).toPromise();
  }

  /**
   * LIVEPARKING (New API)
   */

  getFaq(): Promise<any> {
    return this.http.get(`${this.apiUrl}/faq`).toPromise();
  }

  async addToCityRequestList(city: string, email: string) {
    const response: any = await this.http.post(`${this.apiUrl}/city-requests`, {
      city,
      email
    },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }).toPromise();
    // const response: any = await this.http.get('/assets/json/getOffStreetParkingOptions.json').toPromise();
    return response;
  }

  /**
   * INRIX
   */

  getInrixOnStreetDataByBox(box: any): Promise<any> {
    return this.http.get(`https://parking-api.inrix.com/blocks?box=${box[0][1]}|${box[0][0]},${box[1][1]}|${box[1][0]}&radius=100&sort=distance&limit=1&locale=de-DE&accesstoken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBSb2xlIjozLCJhcHBJZCI6IjNlNzY0NWYxLThmZjktNGVkYy04ZGQ1LWIzZmNlNGIxNjdkNyIsImV4cGlyeSI6IjIwMjAtMDMtMDdUMTM6MjU6NDkuODE0NzY3OVoiLCJleHAiOjE1ODM1ODc1NDksInJvbGUiOiJzZXJ2aWNlIiwiZGV2ZWxvcGVySWQiOiJhOTQ4NGJkYS1jYzFkLTRhN2ItODMwZC1iMmE0NmJiODBjZjcifQ.XHKLd95j-9OP5cNlU5dFvSMblVn2rAN2O0bf0ynM814`).toPromise();
  }

  getInrixOnStreetDataByPoint(lat: number, lng: number): Promise<any> {
    return this.http.get(`https://parking-api.inrix.com/blocks?point=${lat}|${lng}&radius=100&sort=distance&limit=1&locale=de-DE&accesstoken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBSb2xlIjozLCJhcHBJZCI6IjNlNzY0NWYxLThmZjktNGVkYy04ZGQ1LWIzZmNlNGIxNjdkNyIsImV4cGlyeSI6IjIwMjAtMDMtMDdUMTM6MjU6NDkuODE0NzY3OVoiLCJleHAiOjE1ODM1ODc1NDksInJvbGUiOiJzZXJ2aWNlIiwiZGV2ZWxvcGVySWQiOiJhOTQ4NGJkYS1jYzFkLTRhN2ItODMwZC1iMmE0NmJiODBjZjcifQ.XHKLd95j-9OP5cNlU5dFvSMblVn2rAN2O0bf0ynM814`).toPromise();
  }

  /**
   * GOOGLE
   */

  public getGooglePlacesSearchResult(input: string): Promise<any> {
    return this.http.get(`https://cors.js-frameworks.com/?https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${escape(input)}&language=de&components=country:DE|country:CH|country:AT&key=${this.googleApiKey}`).toPromise();
  }

  public getCityByCode(code: any) {
    return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${this.googleApiKey}&address=${code}&language=de`);
  }

  public getCityByAddress(address: string): Promise<object> {
    return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${this.googleApiKey}&address=${address}&language=de`).toPromise();
  }

  public getTransitInfo() {
    return this.http.get('https://maps.googleapis.com/maps/api/directions/json?language=de&origin=Zentralpl.,%2056068%20Koblenz,%20Deutschland&destination=Koblenz%20Hbf,%20Bahnhofpl.%202,%2056068%20Koblenz,%20Deutschland&mode=transit&key=AIzaSyA_1aWJNlsa0zVzlJxfMUvZCxopyYqcYc8');
  }

  /**
   * NAVITIA (Public Transport)
   */

  public getStopAreasNearLocation(location: ILatLng) {
    return this.http.get(`https://api.navitia.io/v1/coverage/de/coord/${location.lng};${location.lat}/stop_areas?distance=1000&count=6`, {
      headers: {
        Authorization: 'Basic ' + btoa(this.navitiaApiKey + ':')
      }
    }).toPromise();
  }

  public getDeparturesOfStopArea(point: string) {

    const nowInFiveMinutes = ((new Date()).getTime() + 5 * 60000);
    const isoTime = new Date(nowInFiveMinutes).toISOString();

    return this.http.get(`https://api.navitia.io/v1/coverage/de/stop_areas/${point}/departures?from_datetime=` + isoTime, {
      headers: {
        Authorization: 'Basic ' + btoa(this.navitiaApiKey + ':')
      }
    }).toPromise();
  }

  /**
   * OPEN CHARGE MAP (Charging Stations)
   */

  getChargingStationsByLatLng(location: ILatLng): Promise<any> {
    return this.http.get(`https://api.openchargemap.io/v3/poi/?output=json&compact=false&countrycode=DE&maxresults=20&distanceunit=KM&latitude=${location.lat}&longitude=${location.lng}`, {
      headers: {
        'X-API-Key': this.openChargeMapApiKey
      }
    }).toPromise();
  }

  public sendFeedback(sender: { name: string; email: string; message: string; }) {

    const headers = new HttpHeaders({ enctype: 'multipart/form-data' });
    const formData = new FormData();
    formData.append('name', sender.name);
    formData.append('email', sender.email);
    formData.append('message', encodeURI(sender.message));

    return this.http.post('https://liveparking.eu/extend/mailer/', formData).toPromise();
  }

  // HELPERS

  /**
   * DO NOT USE IN PRODUCTION!!! - For Development Purposes only.
   */
  getOfferingsForDevelopmentPurposes() {
    return this.http.get('/assets/json/offerings.json').toPromise() as any;
  }


  verifyCouponCode(couponCode: string): Promise<any> {
    return this.http.get(`${environment.apiUrl}/coupons?code=${couponCode}`).toPromise();
  }

  private async showOfflineMessage() {
    const alert = await this.alertController.create({
      header: 'Versuche es später erneut',
      message: 'Leider treten bei der ausgewählten Stadt momentan <b>technische Schwierigkeiten</b> auf. Wir arbeiten bereits an einer Lösung. Bitte versuche es später erneut.',
      buttons: [
        {
          text: 'Support kontaktieren',
          role: 'cancel',
          cssClass: 'secondary',
          handler: async () => {
            // Todo: Insert Suport Page
          }
        }, {
          text: 'Schließen',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  private arrayToString(array: any[]) {
    if (array) {
      let output = '';
      array.forEach(element => {
        output += element + '<br>';
      });
      output = output.replace(/, \s*$/, '');
      return output;
    } else {
      return null;
    }
  }

  private bliqColorNameToHex(colorName: string) {
    switch (colorName) {
      case 'GREEN':
        return '#70bf3d';
      case 'GREEN_YELLOW':
        return '#9cb400';
      case 'YELLOW':
        return '#fdd800';
      case 'ORANGE':
        return '#f25900';
      case 'RED':
        return '#b70000';
      default:
        return '#5f5f5f';
    }
  }

  private bliqColorProbabilityText(colorName: string) {
    switch (colorName) {
      case 'GREEN':
        return 'sehr gute Chance';
      case 'GREEN_YELLOW':
        return 'gute Chance';
      case 'YELLOW':
        return 'moderate Chance';
      case 'ORANGE':
        return 'mäßige Chance';
      case 'RED':
        return 'schlechte Chance';
      default:
        return 'N/a';
    }
  }

  private colorIndicatorByOccupancy(freeSpots: number) {
    if (freeSpots > 50) {
      return '#70bf3d';
    } else if (freeSpots <= 50 && freeSpots > 20) {
      return '#9cb400';
    } else if (freeSpots <= 20 && freeSpots > 10) {
      return '#fdd800';
    } else if (freeSpots <= 10 && freeSpots > 0) {
      return '#f25900';
    } else if (freeSpots === 0) {
      return '#b70000';
    } else {
      return '#5f5f5f';
    }
  }

  private probabilityTextByOccupancy(freeSpots: number) {
    if (freeSpots > 50) {
      return 'sehr gute Chance';
    } else if (freeSpots <= 50 && freeSpots > 20) {
      return 'gute Chance';
    } else if (freeSpots <= 20 && freeSpots > 10) {
      return 'moderate Chance';
    } else if (freeSpots < 10 && freeSpots > 0) {
      return 'mäßige Chance';
    } else if (freeSpots === 0) {
      return 'schlechte Chance';
    } else {
      return '#5f5f5f';
    }
  }

  private isNumber(n: any) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0)
  }

  // We use this whenever we don't want a cached version of a file
  // private guidGenerator() {
  //   var S4 = () => {
  //     return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  //   };
  //   return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
  // }

}

export interface ParkingOptions {
  id?: number | string;
  name?: string;
  freeSpots?: number;
  freeSpotsText?: 'Frei' | 'Besetzt';
  freeSpotsInPercent?: number;
  allSpots?: number;
  handicappedSpots?: number;
  parkingProbability?: number;
  parkingProbabilityText?: string;
  handycapped?: number;
  pricing?: string;
  pricingShort?: string;
  openingTimes?: string;
  paymentMethods?: string;
  features?: any[];
  address?: string;
  extras?: string;
  maxParkingTime?: number;
  maxHeight?: number;
  state?: string; // OPEN / CLOSED
  type?: 'On-Street' | 'Off-Street';
  facilityType?: string;
  indicatorColor?: string;
  location?: {
    lat: number;
    lng: number;
  };
  geometry?: {
    type: 'LineString' | 'Polygon' | 'Point',
    coordinates: any;
  };
  distance?: number;
  distanceByLocation?: number; // Distance to specific target - used for search
}
