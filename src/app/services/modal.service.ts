import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SettingsService } from './settings.service';
import { UpsellModalComponent } from '../components/modals/upsell-modal/upsell-modal.component';
import { CityNotAvailableComponent } from '../components/modals/city-not-available/city-not-available.component';
import { OnboardingModalComponent } from '../components/modals/onboarding-modal/onboarding-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  upsellModalCounter = 1;

  constructor(private modalController: ModalController, private settingsService: SettingsService) { }

  async showOnboarding() {
    const onboardingModal = await this.modalController.create({
      component: OnboardingModalComponent,
      backdropDismiss: false
    });
    await onboardingModal.present();
    return onboardingModal;
  }

  async openUpsellModal() {
    const upsellModal = await this.modalController.create({
      id: 'upsell-modal',
      component: UpsellModalComponent
    });
    await upsellModal.present();
    return upsellModal;
  }

  async openUnavailabilityModal(nearestAvailableCity?: string) {
    const unavailabilityModal = await this.modalController.create({
      id: 'city-unavailable-modal',
      component: CityNotAvailableComponent,
      componentProps: {
        nearestAvailableCity
      }
    });
    await unavailabilityModal.present();
    const { data } = await unavailabilityModal.onDidDismiss();
    const isProUser = await this.settingsService.isProUser();
    if (!data?.hideUpsellModal) {
      this.upsellModalCounter -= 1;
      // Show upsell modal every third time
      if (!isProUser && this.upsellModalCounter === 0) {
        this.upsellModalCounter = 4;
        await this.openUpsellModal();
      }
    }
    return unavailabilityModal;
  }
}

