import { Injectable } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor() { }


  getLiveChart(free: number, capacity: number, description: string, suffix: string = '') {

    const options: any = {
      chart: {
        type: 'solidgauge',
        width: window.innerWidth, // -40 = ion-card margin left + right
        height: 180,
        zoomType: null,
        renderTo: 'container',
        exporting: false,
        margin: [0, 0, 0, 0],
        backgroundColor: 'transparent',
      },

      title: {
        text: (free === null || free === undefined ? '?' : free.toString() + suffix),
        align: 'center',
        verticalAlign: 'middle',
        y: 28,
        style: {
          color: 'rgba(255, 255, 255, 1)',
          fontSize: '36pt',
          fontWeight: 200,
        }
      },

      subtitle: {
        text: description,
        align: 'center',
        verticalAlign: 'middle',
        y: 30,
        style: {
          color: '#c4c4c4',
          fontSize: (description.length > 16) ? '9pt' : '10pt',
          textTransform: 'uppercase'
        }
      },

      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: -120,
        endAngle: 120,
        background: {
          backgroundColor: 'rgba(0, 0, 0, 0.2)',
          borderColor: '#fffff',
          innerRadius: '90%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },

      tooltip: {
        enabled: false,
        followPointer: false,
        followTouchMove: false,
      },

      // the value axis
      yAxis: {
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 20,
        tickLength: 4,
        tickWidth: 1,
        tickColor: 'grey',
        tickPosition: 'inside',
        stops: [
          [0.5, {
            linearGradient: {
              x1: 0,
              x2: 0,
              y1: 0,
              y2: 1
            },
            stops: [
              // [0, 'rgba(47, 130, 226, .8)'],
              // [1, 'rgba(114, 107, 236, .8)']
              [0, 'rgba(255, 255, 255, 1)'],
              [1, 'rgba(255, 255, 255, 1)']
            ]
          }],
          [0.9, {
            linearGradient: {
              x1: 0,
              x2: 1,
              y1: 0,
              y2: 0
            },
            stops: [
              // [0, 'rgba(47, 130, 226, .8)'],
              // [0.5, 'rgba(114, 107, 236, .8)'],
              // [1, 'rgba(47, 130, 226, .8)']
              [0, 'rgba(255, 255, 255, 1)'],
              [0.5, 'rgba(255, 255, 255, 1)'],
              [1, 'rgba(255, 255, 255, 1)']
            ]
          }]
        ],
        title: null,
        labels: {
          enabled: false
        },
        min: 0,
        max: 100,
      },

      plotOptions: {
        solidgauge: {
          innerRadius: '90%',
          linecap: 'round',
          stickyTracking: false,
          rounded: true,
          dataLabels: {
            enabled: false
          }
        }
      },
      credits: {
        enabled: false
      },

      series: [{
        name: 'Speed',
        data: [free / capacity * 100],
        dataLabels: {
          enabled: false
        },
      }],
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
    };

    return new Chart(options);
  }

  getForecastChart(data: any) {

    const options: any = {
      chart: {
        zoomType: null,
        renderTo: 'container',
        type: 'line',
        margin: 0,
        width: window.innerWidth - 40,
        height: 200,
      },
      credits: {
        enabled: false
      },
      title: {
        text: '',
        align: 'center',
        verticalAlign: 'middle',
        y: 40
      },
      tooltip: {
        followPointer: false,
        followTouchMove: false,
      },
      plotOptions: {
        pie: {
          shadow: false,
          enableMouseTracking: false,
          // center: ['50%', '75%'],
          // size: '110%'
        }
      },
      series: [{
        name: 'Frei',
        data,
        size: '100%',
        innerSize: '90%',
        showInLegend: false,
        dataLabels: {
          enabled: false
        },
        color: {
          linearGradient: [0, 0, 0, 500],
          stops: [
            [0, 'rgb(255, 255, 255)'],
            [1, 'rgb(100, 100, 155)']
          ]
        }
      }]
    };

    return new Chart(options);
  }

  getMiniChart(free: any, capacity: number) {

    let color: string;
    let y = 8;
    let fontSize = '12pt';
    let percentage = free / capacity * 100;

    switch (free) {
      case (free >= 20):
        color = '#61C842'; // Green
        break;
      case (free <= 19 && free > 0):
        color = '#ffbd2f'; // Yellow
        break;
      case (free === 0):
        color = '#df4d19'; // Red
        break;
      default:
        color = '#5f5f5f'; // Grey
    }

    percentage = free / capacity * 100;

    // Check for strings
    switch (free) {
      case (free > 999):
        y = 6;
        fontSize = '9pt';
        break;
      case (free === 'frei'):
        color = '#61C842'; // Green
        percentage = 100;
        break;
      case (free === 'besetzt'):
        y = 6;
        fontSize = '8pt';
        color = '#df4d19'; // Red
        percentage = 100;
        break;
    }

    const options: any = {
      chart: {
        type: 'solidgauge',
        width: 50, // -40 = ion-card margin left + right
        height: 50,
        zoomType: null,
        renderTo: 'container',
        exporting: false,
        margin: [0, 0, 0, 0],
        backgroundColor: 'transparent'
      },

      title: {
        text: free === null ? '?' : free.toString(),
        align: 'center',
        verticalAlign: 'middle',
        y,
        style: {
          color,
          fontSize,
          fontWeight: 200,
        }
      },

      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: 0,
        endAngle: 360,
        background: {
          backgroundColor: '#f6f6f6',
          borderColor: '#fffff',
          innerRadius: '90%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },

      tooltip: {
        enabled: false,
        followPointer: false,
        followTouchMove: false,
      },

      // the value axis
      yAxis: {
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 0,
        tickLength: 0,
        tickWidth: 0,
        tickColor: 'grey',
        tickPosition: 'inside',
        stops: [
          [0.5, {
            linearGradient: {
              x1: 0,
              x2: 0,
              y1: 0,
              y2: 1
            },
            stops: [
              // [0, 'rgba(47, 130, 226, .8)'],
              // [1, 'rgba(114, 107, 236, .8)']
              [0, color],
              [1, color]
            ]
          }],
          [0.9, {
            linearGradient: {
              x1: 0,
              x2: 1,
              y1: 0,
              y2: 0
            },
            stops: [
              // [0, 'rgba(47, 130, 226, .8)'],
              // [0.5, 'rgba(114, 107, 236, .8)'],
              // [1, 'rgba(47, 130, 226, .8)']
              [0, color],
              [0.5, color],
              [1, color]
            ]
          }]
        ],
        title: {
          y: -70
        },
        labels: {
          enabled: false
        },
        min: 0,
        max: 100,
      },

      plotOptions: {
        solidgauge: {
          innerRadius: '90%',
          linecap: 'round',
          stickyTracking: false,
          rounded: true,
          animation: false,
          dataLabels: {
            enabled: false
          }
        }
      },
      credits: {
        enabled: false
      },

      series: [{
        name: 'Speed',
        data: [percentage],
        dataLabels: {
          enabled: false
        },
      }],
      navigation: {
        buttonOptions: {
          enabled: false
        }
      }
    };

    return new Chart(options);
  }
}

