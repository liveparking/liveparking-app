import { Component, OnInit } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { MapService } from '../../services/map.service';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@Component({
  selector: 'location-error-modal',
  templateUrl: './location-error-modal.component.html',
  styleUrls: ['./location-error-modal.component.scss'],
})
export class LocationErrorModalComponent implements OnInit {

  locationCheckInterval: any;
  lottieOpts = {
    path: '/assets/lottie/1342-location.json'
  };

  constructor(
    private platform: Platform,
    private openNativeSettings: OpenNativeSettings,
    private mapService: MapService,
    private modalController: ModalController) { }

  ngOnInit() {
    if (this.platform.is('cordova')) {
      this.locationCheckInterval = setInterval(async () => {
        await this.checkIfLocationServiceIsEnabled();
      }, 1000);
    }
  }

  async openSettings() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.openNativeSettings.open('location');
    }
  }

  async checkIfLocationServiceIsEnabled() {
    try {
      await this.mapService.getUserLatLng();
      await this.modalController.dismiss();
    } catch (e) {
      console.log('Locations Services still disabled');
    }
  }

  ionViewWillLeave() {
    clearInterval(this.locationCheckInterval);
  }

}
