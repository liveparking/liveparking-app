import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { MomentModule } from 'ngx-moment';
import { CarouselItemComponent } from './carousel-item/carousel-item.component';
import { UpsellCardComponent } from './upsell-card/upsell-card.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MomentModule,
    FilterPipeModule,
    IonicModule.forRoot(),
  ],
  declarations: [
    CarouselItemComponent,
    UpsellCardComponent
  ],
  exports: [
    CarouselItemComponent,
    UpsellCardComponent
  ]
})
export class ComponentsModule {}
