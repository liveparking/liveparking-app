import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ParkingOptions } from '../../services/rest.service';
import { isNumber } from 'util';

@Component({
  selector: 'carousel-item',
  templateUrl: './carousel-item.component.html',
  styleUrls: ['./carousel-item.component.scss'],
})
export class CarouselItemComponent implements OnInit {

  isFavorite = false;
  svgContent: string;
  @Input() input: ParkingOptions;

  showChance = false;
  showFreeSpots = false;
  showNoData = false;
  showText = false;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {

    if (this.isNumber(this.input.freeSpots)) {
      this.showFreeSpots = true;
    } else if (this.isNumber(this.input.parkingProbability)) {
      this.showChance = true;
    } else if (!this.isNumber(this.input.freeSpots) && !this.isNumber(this.input.parkingProbability) && !this.input.freeSpotsText) {
      this.showNoData = true;
    } else if (!this.isNumber(this.input.freeSpots) && !this.isNumber(this.input.parkingProbability) && this.input.freeSpotsText) {
      this.showText = true;
    }

    this.svgContent = `
    <svg viewBox="0 0 36 36" style="display: block;
    margin: 0;
    max-width: 140px;
    max-height: 140px;">
    <path style="fill: none;
    stroke: #F8F8F8;
    stroke-width: 2.8;"
      d="M18 2.0845
        a 15.9155 15.9155 0 0 1 0 31.831
        a 15.9155 15.9155 0 0 1 0 -31.831"
    />
    <path style="fill: none;
    stroke-width: 2.8;
    stroke-linecap: round;
    stroke: ${this.input.indicatorColor};
    animation: progress 2.5s ease-out forwards;"
    stroke-dasharray="${this.input.freeSpots ? ((this.input.freeSpots / this.input.allSpots) * 100) : 0}, 100"
      d="M18 2.0845
        a 15.9155 15.9155 0 0 1 0 31.831
        a 15.9155 15.9155 0 0 1 0 -31.831"
    />
  </svg>`;
    // this.item = this.input;
  }

  sanitize(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  isNumber(n: any) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0)
  }
}
