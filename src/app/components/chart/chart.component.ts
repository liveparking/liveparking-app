import { Component, AfterViewInit, Input } from '@angular/core';
import { ChartService } from '../../services/chart.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewInit {

  chart: any;

  @Input() public free: number;
  @Input() public capacity: number;
  @Input() public type: string;
  @Input() public description: string;
  @Input() public suffix: string;

  constructor(private chartService: ChartService) { }

  ngAfterViewInit() {
    if (this.type === 'small') {
      this.chart = this.chartService.getMiniChart(this.free, this.capacity);
    } else if (this.type === 'large') {
      this.chart = this.chartService.getLiveChart(this.free, this.capacity, this.description, this.suffix);
    } else if (this.type === 'forecast') {

    }

  }

}
