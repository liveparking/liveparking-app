import { Component, Input, Output, ViewChild, OnInit, NgZone, EventEmitter } from '@angular/core';
import { IonSearchbar, Platform } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { ILatLng, MapService } from '../../services/map.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {

  searchResults = null;
  loadingActive = false;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSearchResultClick: EventEmitter<ILatLng> = new EventEmitter<ILatLng>(); // Used to expose position to parent component
  @Output() onFocus: EventEmitter<null> = new EventEmitter<null>();

  @Input() public disabled: false;
  @ViewChild('searchbar', { static: false }) searchbar: IonSearchbar;

  constructor(
    private restService: RestService,
    private ngZone: NgZone,
    private platform: Platform,
    private mapService: MapService,
    private keyboard: Keyboard) { }

  ngOnInit() {
    this.subscribeToKeyboardEvents();
  }

  // This function will be triggered after user input
  async search(e: CustomEvent) {
    if (e.detail.value.length) {
      this.searchResults = null; // Hide search results window
      this.showLoading(true);
      const response = await this.restService.getGooglePlacesSearchResult(e.detail.value);
      this.searchResults = response.predictions;
      this.showLoading(false);
    }
  }

  private showLoading(value: boolean) {
    this.ngZone.run(() => {
      this.loadingActive = value;
    })
  }

  // Will be executed on search result click
  async moveMapToSearchResult(searchResult: any) {
    // Copy clicked search result into search bar (UX)
    const inputElement = await this.searchbar.getInputElement();
    inputElement.value = searchResult?.description;
    // Hide search results window
    this.searchResults = null;
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      // Convert search result term to lat long
      const result = await this.mapService.addressToLatLng(searchResult?.description);
      // Set variable to use in HomePage
      this.onSearchResultClick.emit(result[0].position);
      // Clear old markers, etc...
      await this.mapService.clearMap();
      // Set marker
      await this.mapService.markPosition('searchResult', result[0].position);
      // Move map to the search result's location
      await this.mapService.animateCamera(result[0].position, 18, 500);
    }
  }

  clearSearch() {
    this.searchResults = null; // Hide search results window
    if (this.platform.is('cordova')) {
      this.keyboard.hide();
    }
  }

  onFocusEvent() {
    this.onFocus.emit();
  }

  /**
   * Keyboard Events are necessary to deactivate the touch events of the map
   * The is better UX for the user while he's interacting with the search results
   */
  private subscribeToKeyboardEvents() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.keyboard.onKeyboardWillHide().subscribe(() => {
          // Hide search results if user clicks on map to close keyboard
          this.searchResults = null;
          this.mapService.setClickable(true);
        });
        this.keyboard.onKeyboardWillShow().subscribe(() => {
          this.mapService.setClickable(false);
        });
      }
    });
  }

}
