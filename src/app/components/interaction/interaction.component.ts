import { Component, OnInit } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.scss'],
})
export class InteractionComponent implements OnInit {

  constructor(
    private platform: Platform,
    private storage: StorageService,
    public modalCtrl: ModalController,
    public launchNavigator: LaunchNavigator) { }

  ngOnInit() { }

  async navigate() {
    await this.platform.ready();
    const position = await this.storage.get('parkingPos');
    const options: LaunchNavigatorOptions = {
      appSelection: {
        dialogHeaderText: 'App für Navigation wählen',
        cancelButtonText: 'Abbrechen',
        rememberChoice: {
          enabled: false
        }
      }
    };
    if (this.platform.is('cordova')) {
      this.launchNavigator.navigate([position.lat, position.lng], options)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );
    }
  }

  delete() {
    this.storage.remove('parkingPos');
    this.modalCtrl.dismiss();
  }

}
