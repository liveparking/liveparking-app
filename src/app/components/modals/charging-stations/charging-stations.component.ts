import { Component, OnInit, Input } from '@angular/core';
import { Platform, ModalController, ToastController } from '@ionic/angular';
import { RestService } from '../../../services/rest.service';
import { MapService, ILatLng } from '../../../services/map.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-charging-stations',
  templateUrl: './charging-stations.component.html',
  styleUrls: ['./charging-stations.component.scss'],
})
export class ChargingStationsComponent implements OnInit {

  @Input() location: ILatLng;

  chargingStations = [];
  chargingStationsAvailable = true;
  manualRefresh = false; // Hide ion-spinner tag

  constructor(
    private restService: RestService,
    private mapService: MapService,
    private platform: Platform,
    private modalController: ModalController,
    private toastController: ToastController,
    private launchNavigator: LaunchNavigator) { }

  async ngOnInit() {
    await this.getChargingStations();
  }

  async getChargingStations() {
    this.chargingStationsAvailable = true;
    this.chargingStations = [];
    const response = await this.restService.getChargingStationsByLatLng(this.location);

    try {
      for (const item of response) {
        item.AddressInfo.Distance = await this.mapService.calcDistance({
          lat: Number(item.AddressInfo.Latitude),
          lng: Number(item.AddressInfo.Longitude)
        });
      }
    } catch {
      // Location services are disabled
      // Remove the distance calculated by the API, because it calculates the distance from the parking option to the charging station.
      // But we alway calculate the distance from the exact position of the user to the final destination.
      // We don't want to confuse the user, so we remove the wrongly calculated distance that was calculated by the API.
      response.map(item => item.AddressInfo.Distance = null);
    }

    this.chargingStations = response;

    if (!this.chargingStations.length) {
      this.chargingStationsAvailable = false;
    }
  }

  async refreshDetails(event?: any) {
    this.manualRefresh = true;
    try {
      this.chargingStations = await this.restService.getChargingStationsByLatLng(this.location);
    } catch {
      const toast = await this.toastController.create({
        message: 'Fehler beim Aktualisieren.',
        duration: 2000,
        cssClass: 'custom-toast'
      });
      toast.present();
    } finally {
      event.target.complete();
      this.manualRefresh = false;
    }
  }

  async openNavigator(location: ILatLng, destination?: string) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {

      const options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: destination ? 'Navigieren zu ' + destination : 'App für Navigation wählen',
          cancelButtonText: 'Abbrechen',
          rememberChoice: {
            enabled: false
          }
        }
      };
      try {
        await this.launchNavigator.navigate([location.lat, location.lng], options);
      } catch (e) {
        console.log(e);
      }
    }
  }

  async close() {
    await this.modalController.dismiss();
  }

}
