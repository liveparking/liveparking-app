import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { SettingsService } from '../../../services/settings.service';
import { RestService } from '../../../services/rest.service';
import { MapService } from '../../../services/map.service';
import { slideTransitionSimple } from '../../../transitions/page-transition';

@Component({
  selector: 'app-city-not-available',
  templateUrl: './city-not-available.component.html',
  styleUrls: ['./city-not-available.component.scss'],
})
export class CityNotAvailableComponent implements OnInit {

  isProUser = false;
  @Input() nearestAvailableCity: string;

  constructor(
    private modalController: ModalController,
    private navController: NavController,
    private settingsService: SettingsService,
    private alertController: AlertController,
    private restService: RestService,
    private mapService: MapService,
    private platform: Platform,
    private loadingController: LoadingController) { }

  async ngOnInit() {
    this.isProUser = await this.settingsService.isProUser();
  }

  async navigateToCityList() {
    await this.navController.navigateForward('/citylist', {
      animation: slideTransitionSimple
    });
    await this.modalController.dismiss({
      hideUpsellModal: true
    });
  }

  async requestCity() {
    await this.modalController.dismiss({
      hideUpsellModal: true
    });
    const city = this.platform.is('cordova') ? await this.mapService.getCityMetaByMapPosition() : 'Demo Stadt';
    const alert = await this.alertController.create({
      header: 'Stadt anfordern',
      subHeader: city?.locality + ', ' + city?.country,
      message: 'Gerne versuchen wir ' + city?.locality + ' zu integrieren. Bitte verrate uns Deine E-Mail Adresse, damit wir Dich bzgl. des Fortschritts informieren können. Dies ist keine Newsletteranmeldung.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Deine E-Mail Adresse'
        },
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel'
        },
        {
          text: 'Senden',
          handler: async (input) => {
            try {
              await this.restService.addToCityRequestList(city?.locality + ', ' + city?.country, input.email);
            } catch (e){
              console.error(e);
            }
            const loading = await this.loadingController.create({
              message: 'Danke',
              spinner: null,
              duration: 1000
            });
            loading.present();
          }
        }
      ]
    });
    await alert.present();
  }

  async close() {
    await this.modalController.dismiss();
  }

}
