import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { MapService, ILatLng } from '../../../services/map.service';
import { RestService } from '../../../services/rest.service';
import { OrderPipe } from 'ngx-order-pipe';

@Component({
  selector: 'app-public-transport',
  templateUrl: './public-transport.component.html',
  styleUrls: ['./public-transport.component.scss'],
})
export class PublicTransportComponent implements OnInit {

  @Input() location: ILatLng;

  stopAreas = [];
  stopAreasAvailable = true;
  departures = [];
  departuresAvailable = true;
  selectedStopArea: any;
  manualRefresh = false; // Hide ion-spinner tag

  constructor(
    private orderPipe: OrderPipe,
    private restService: RestService,
    private mapService: MapService,
    private toastController: ToastController,
    private modalController: ModalController) { }

  async ngOnInit() {
    await this.getStopAreas(this.location);
  }

  async getStopAreas(location: ILatLng) {

    this.stopAreasAvailable = true; // Reset

    try {
      const resp: any = await this.restService.getStopAreasNearLocation(location);
      const stopAreas = resp.stop_areas;

      if (this.selectedStopArea === undefined) {
        this.selectedStopArea = stopAreas[0];
      }

      for (const item of stopAreas) {
        item.name = item.name.split(',')[1] || item.name // ex.: make "Köln, Heumarkt" to "Heumarkt" if possible
      }

      try {
        for (const item of stopAreas) {
          item.distance = await this.mapService.calcDistance({
            lat: Number(item.coord.lat),
            lng: Number(item.coord.lon)
          });
        }
        this.stopAreas = this.orderPipe.transform(stopAreas, 'distance');
      } catch {
        // Location services are disabled
        this.stopAreas = this.orderPipe.transform(stopAreas, 'name');
      }

      await this.getDepartures();
    } catch {
      this.stopAreasAvailable = false;
      this.departuresAvailable = false;
    }

  }

  async getDepartures() {

    this.departuresAvailable = true; // Reset
    this.departures = [];

    const resp: any = await this.restService.getDeparturesOfStopArea(this.selectedStopArea.id);
    this.departures = resp.departures;

    if (!this.departures.length) {
      this.departuresAvailable = false;
    }
  }

  async refreshDetails(event?: any) {
    this.manualRefresh = true;
    try {
      const resp: any = await this.restService.getDeparturesOfStopArea(this.selectedStopArea.id);
      this.departures = resp.departures;
    } catch {
      const toast = await this.toastController.create({
        message: 'Fehler beim Aktualisieren.',
        duration: 2000,
        cssClass: 'custom-toast'
      });
      toast.present();
    } finally {
      event.target.complete();
      this.manualRefresh = false;
    }
  }

  async changeStopArea(e: CustomEvent) {

    const stopAreaId = e.detail.value;
    this.departures = [];

    this.selectedStopArea = this.getStopAreaById(stopAreaId);

    await this.getDepartures();
  }

  getStopAreaById(id: string) {
    let area: object;
    this.stopAreas.forEach((item, i) => {
      if (item.id === id) {
        area = item;
      }
    });
    return area;
  }

  async close() {
    await this.modalController.dismiss();
  }
}

