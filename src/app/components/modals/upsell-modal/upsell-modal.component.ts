import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { slideTransitionSimple } from '../../../transitions/page-transition';

@Component({
  selector: 'app-upsell-modal',
  templateUrl: './upsell-modal.component.html',
  styleUrls: ['./upsell-modal.component.scss'],
})
export class UpsellModalComponent implements OnInit {

  features = [
    '500+ unterstützte Städte',
    'Freie Parkplätze auf Parkstreifen',
    'Durch künstliche Intelligenz gestützt',
    'Komplett werbefrei'
  ]

  constructor(
    private modalController: ModalController,
    private navController: NavController) { }

  ngOnInit() { }

  async openBuyPage() {
    await this.navController.navigateForward('/buy-pro', {
      animation: slideTransitionSimple
    });
    await this.modalController.dismiss();
  }

  async close() {
    await this.modalController.dismiss();
  }

}
