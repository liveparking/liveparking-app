import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, ModalController } from '@ionic/angular';
import { StorageService } from '../../../services/storage.service';
import { MapService } from '../../../services/map.service';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-onboarding-modal',
  templateUrl: './onboarding-modal.component.html',
  styleUrls: ['./onboarding-modal.component.scss'],
})
export class OnboardingModalComponent implements OnInit {

  termsAccepted = false;
  locationServicesEnabled = false;

  features = [
    'Sei schneller am Ziel',
    'Spare Sprit, Zeit & Geld',
    'Keine lästige Parkplatzsuche',
    'Schont die Umwelt'
  ];

  constructor(
    private platform: Platform,
    private safariViewController: SafariViewController,
    private inAppBrowser: InAppBrowser,
    private alertController: AlertController,
    private mapService: MapService,
    private modalController: ModalController,
    private storage: StorageService
  ) { }

  async ngOnInit() { }

  async activateLocationServices() {
    try {
      await this.mapService.getUserLatLng();
    } catch (e) {
      console.error(e)
      // if (this.locationServicesEnabled) { // prevent the alert to open two times
      //   this.locationServicesEnabled = false; // change back toggle
      //   const alert = await this.alertController.create({
      //     header: 'Manuell aktivieren',
      //     message: 'Leider können wir die Ortungsdienste nicht aktivieren. Bitte mache dies über die Einstellungen Deines Gerätes.',
      //     buttons: ['Schließen']
      //   });
      //   await alert.present();
      // }
    }
  }

  async noticeActivateLocationServices() {
    const alert = await this.alertController.create({
      header: 'Ortungsdienste aktivieren?',
      message: 'Wir empfehlen Dir die Ortungsdienste zu aktivieren, da wir sonst nicht automatisch Parkmöglichkeiten in der Nähe finden können.',
      buttons: [
        {
          text: 'Weiter',
          role: 'cancel',
          handler: async () => {
            await this.close();
          }
        },
        {
          text: 'Ortungsdienste aktivieren',
          handler: async () => {
            this.locationServicesEnabled = true;
            await this.activateLocationServices();
          }
        }
      ]
    });
    await alert.present();
  }

  async showLocationServicesNotice() {
    const alert = await this.alertController.create({
      header: 'Ortungsdienste',
      message: 'Um die aktuelle Distanz zu den verschiedenen Parkmöglichkeiten zu berechnen benötigen wir Zugriff auf die Ortungsdienste Deines Gerätes. Dein Standort wird ausschließlich Verwendung der App abgefragt.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async openTerms() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.safariViewController.show({
        url: 'https://liveparking.eu/legal/terms/',
        tintColor: '#4F536A'
      }).toPromise();
    }
  }

  writeEmail() {
    this.platform.ready().then(() => {
      this.inAppBrowser.create(`mailto:${environment.supportEmail}`, '_system');
    });
  }

  async close() {
    await this.storage.set('isFirstLaunch', false);
    await this.modalController.dismiss();
  }

}
