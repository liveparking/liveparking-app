import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'upsell-card',
  templateUrl: './upsell-card.component.html',
  styleUrls: ['./upsell-card.component.scss'],
})
export class UpsellCardComponent implements OnInit {

  @Input() showShadow = false;
  @Input() showBorder = false;
  @Input() showImage = false;
  @Input() showDescription = false;
  @Input() subtitle: string;
  @Input() disabled = false;

  constructor() { }

  ngOnInit() {
    if (this.subtitle) {
      this.subtitle = this.subtitle.toUpperCase();
    }
  }

}
