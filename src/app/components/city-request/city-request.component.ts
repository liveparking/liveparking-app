import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, AlertController, NavController, IonInput } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { environment } from '../../../environments/environment';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'city-request',
  templateUrl: './city-request.component.html',
  styleUrls: ['./city-request.component.scss'],
})
export class CityRequestComponent implements OnInit {

  loadingActive = false;
  userCityName: string;
  userEmail: string;
  evaluatedCity: string;
  slideOpts = {
    speed: 400,
    allowTouchMove: false,
    autoHeight: true,
    zoom: false
  };
  errorMessage = {
    email: ''
  };

  @ViewChild('email', { static: false }) emailInput: IonInput;
  @ViewChild('slides', { static: true }) slides: IonSlides;

  constructor(
    private keyboard: Keyboard,
    private alertController: AlertController,
    private restService: RestService,
    private navController: NavController) { }

  ngOnInit() { }

  async checkCityRequestInput() {
    try {
      this.loadingActive = true;
      const response: any = await this.restService.getCityByAddress(this.userCityName);
      const cityName: string = response.results[0]?.formatted_address;
      if (cityName) {
        this.evaluatedCity = cityName;
        this.keyboard.hide();
        await this.slides.slideNext();
      } else {
        const alert = await this.alertController.create({
          header: 'Stadt nicht gefunden',
          message: 'Diese Stadt konnten wir leider nicht finden. Evtl. hilft es, eine andere Schreibweise zu verwenden.',
          buttons: ['OK']
        });
        await alert.present();
      }
    } catch {
      await this.unknownError();
    } finally {
      this.loadingActive = false;
    }
  }

  async resultWrong() {
    await this.slides.slidePrev();
  }

  async resultRight() {
    await this.slides.slideNext();
    setTimeout(async () => {
      await this.emailInput.setFocus();
    }, 100);
  }

  async submitForm() {
    if (await this.validateEmail()) {
      try {
        this.loadingActive = true;
        const response: any = await this.restService.addToCityRequestList(this.evaluatedCity, this.userEmail);
        if(response?.error) {
          await this.unknownError();
        } else {
          this.keyboard.hide();
          await this.slides.slideNext();
          this.resetVariables();
        }
      } catch {
        await this.unknownError();
      } finally {
        this.loadingActive = false;
      }
    }
  }

  async infoCityRequest() {
    const alert = await this.alertController.create({
      message: 'Wir möchten so viele Städte wie möglich kostenlos anbieten. Dazu müssen wir mit den jeweiligen Städten Rücksprache halten, was sehr Zeitaufwendig und nicht immer möglich ist. Für die Städte, die Du momentan in der PRO Version findest müssen wir die Daten selbst einkaufen, weswegen wir diese nicht kostenlos anbieten können. Wenn die Stadt, in der Du wohnst ein Parkleitsystem besitzt und die Daten evtl. schon digitalisiert hat, kannst Du uns dies gerne mitteilen und wir schauen, ob wir die Stadt ggf. in die kostenlose Version von LiveParking aufnehmen können.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async emailInfo() {
    const alert = await this.alertController.create({
      header: 'Warum benötigen wir Deine E-Mail Adresse?',
      message: 'Mit dem Absenden des Formulars erklärst Du Dich einverstanden, ggf. E-Mails in Bezug auf die Integration der gewünschten Stadt zu erhalten. Gerne darfst Du Dich auch für unseren Newsletter anmelden, um über neue Funktionen informiert zu sein. Wir senden nur ganz selten E-Mails, versprochen!',
      buttons: ['OK']
    });
    await alert.present();
  }

  async openContactForm() {
    await this.navController.navigateForward('/feedback-form');
  }

  resetVariables() {
    this.userCityName = '';
    this.userEmail = '';
  }

  async navigateToFaq() {
    await this.navController.navigateForward('/faq');
  }

  async unknownError() {
    const alert = await this.alertController.create({
      header: 'Fehler',
      message: 'Irgendwas scheint da schiefgelaufen zu sein. Bitte versuche es später erneut.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async validateEmail(): Promise<boolean> {
    const re = /^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.(de|at|ch|com|net|org|com|be|info|biz|edu|eu|fr|nl|pl|it|lu|li|co|us|ventures|app)$/;
    const validEmail = re.test(this.userEmail);
    if (validEmail) {
      return true;
    } else {
      const alert = await this.alertController.create({
        header: 'E-Mail ungültig',
        message: 'Bitte gib eine gültige E-Mail Adresse an.',
        buttons: ['OK']
      });
      await alert.present();
      return false;
    }
  }
}
