import { Component } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { LaunchReview } from '@ionic-native/launch-review/ngx';

@Component({
  selector: 'app-rating-modal',
  templateUrl: './rating-modal.component.html',
  styleUrls: ['./rating-modal.component.scss'],
})
export class RatingModalComponent {

  constructor(
    private platform: Platform,
    private modalController: ModalController,
    private launchReview: LaunchReview) { }

  async rateApp() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      if (this.launchReview.isRatingSupported()) {
        await this.launchReview.rating();
      } else {
        await this.launchReview.launch();
      }
    }
    await this.close();
  }

  async close() {
    await this.modalController.dismiss();
  }

}
