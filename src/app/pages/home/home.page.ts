import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, NavController, Platform, AlertController, mdTransitionAnimation } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { OrderPipe } from 'ngx-order-pipe';
import { RestService, ParkingOptions, SettingsService, MapService, ILatLng, ModalService, PurchaseService } from '../../services';
import { slideTransitionSimple } from '../../transitions/page-transition';
import { SearchbarComponent } from '../../components/searchbar/searchbar.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  distanceInterval: any;
  isProUser = false;
  rest: ParkingOptions[] = [];
  distanceCalculationInterval: Subscription;
  onCameraMoveSubscription: Subscription;
  activeSlideIndex = 0;
  searchResults = null; // null = hidden
  loadingActive = false;
  noResults = false;
  zoomNotice = false;
  originLocation: ILatLng; // used to determine order of results

  @ViewChild('locations') slides: IonSlides;
  @ViewChild('searchbar') private searchbar: SearchbarComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private platform: Platform,
    private ngZone: NgZone,
    private restService: RestService,
    private settings: SettingsService,
    private mapService: MapService,
    private orderPipe: OrderPipe,
    private navCtrl: NavController,
    private alertController: AlertController,
    private purchaseService: PurchaseService,
    private modalService: ModalService) { }

  async ngOnInit() {

    this.subscribeToQueryParams();
    this.subscribeToCustomEvents();

    this.isProUser = await this.settings.isProUser();

    // Check if onboarding should show up
    if (await this.settings.isFirstLaunch()) {
      const onboardingModal = await this.modalService.showOnboarding();
      await onboardingModal.onWillDismiss();
    }

    await this.initMap();
    await this.checkUserSubscriptionStatus();
  }

  private async initMap() {

    await this.platform.ready();
    if (this.platform.is('cordova')) {

      this.showLoading(true);
      await this.mapService.initMap();
      this.showLoading(false);

      // Apply map settings
      if (await this.settings.trafficEnabled()) {
        await this.mapService.setTrafficEnabled(true);
      }

      let previousCameraPosition: ILatLng;
      // Prevent multiple possible subscriptions
      if (!this.onCameraMoveSubscription) {
        this.onCameraMoveSubscription = this.mapService.onCameraMoveEnd().subscribe(async data => {
          // Only load data if user has zoomed in
          if (data[0].zoom < 17) {
            // Inform user to zoom more
            this.showLoading(false);
            this.showZoomNotice(true);
            return;
          }

          this.showZoomNotice(false);
          const currentCameraPosition = data[0].target;
          // If previousPosition is available, check if user moved map enough to load more data
          if (previousCameraPosition) {
            const distance = await this.mapService.calcDistanceBetweenPoints(previousCameraPosition, currentCameraPosition);
            if (distance > 150) {
              previousCameraPosition = currentCameraPosition;
              await this.loadData();
            }
          }
        });
      }
      // If previousPosition isn't available, like on init, load the first set of data
      if (!previousCameraPosition) {
        try {
          await this.mapService.showUserLocation();
          const latLng = this.mapService.getCameraPosition();
          previousCameraPosition = latLng;
        } catch {
          const latLng = this.mapService.getCameraPosition();
          if (latLng) {
            previousCameraPosition = latLng;
          } else {
            // Fallback
            previousCameraPosition = {
              lat: 51.165691,
              lng: 10.451526
            }; // Middle of germany
          }
        }
      }
      this.startDistanceRecalculationInterval();
    }
  }

  private async loadData() {
    let onStreet: ParkingOptions[] = [];
    let offStreet: ParkingOptions[] = [];

    this.showResultsNotice(false);
    this.showZoomNotice(false);
    this.showLoading(true);

    offStreet = await this.getOffStreetData('liveparking');
    await this.mapService.setMarkers(offStreet);

    if (this.isProUser) {
      onStreet = await this.getOnStreetData();
      await this.mapService.addElementsToMap(onStreet);
      if (offStreet.length === 0) {
        offStreet = await this.getOffStreetData('bliq');
        await this.mapService.addElementsToMap(offStreet);
      }
    }

    const curatedData = [...onStreet, ...offStreet];

    // Remove objects from response that already exist in this.rest to prevent doubled entries
    const parkingOptions = curatedData.filter(newItem => {
      const alreadyInArray = this.rest.some(existingItem => existingItem.id === newItem.id);
      return alreadyInArray ? false : true;
    });

    try {
      const itemsWithDistance = await this.calcDistance([...this.rest, ...parkingOptions], this.originLocation);

      // Show info if no results are available or if they're more than 2 kilometers away
      if (!itemsWithDistance.length || itemsWithDistance.length && itemsWithDistance[0].distanceByLocation > 2000) {
        this.showLoading(false);
        this.showZoomNotice(false);
        if (this.isProUser) {
          this.showResultsNotice(false);
          const city = itemsWithDistance.length ? await this.mapService.latLngToAddress(itemsWithDistance[0].location) : null;
          await this.modalService.openUnavailabilityModal(city ? city[0].locality : null);
        } else {
          this.showResultsNotice(true);
        }
        return;
      }

      this.rest = this.orderPipe.transform(itemsWithDistance, 'distanceByLocation') as ParkingOptions[];
    } catch {
      this.rest = this.orderPipe.transform([...this.rest, ...parkingOptions], 'name') as ParkingOptions[];
    }
    await this.slides.update();
    this.showLoading(false);
    this.showResultsNotice(false);
    this.showZoomNotice(false);
  }

  /**
   * Returns on-street parking options by current map position
   * Items that are already present in this.rest will be sorted out
   * @param provider
   */
  private async getOnStreetData(provider?: 'bliq') {
    try {
      const location = this.mapService.getCameraPosition();
      return !(await this.settings.trafficEnabled()) ? await this.restService.getOnStreetParking(location.lat, location.lng) : [];
    } catch (e) {
      console.error('Couldn\'t load on street parking', e);
      return [];
    }
  }

  /**
   * Returns on-street parking options by current map position
   * Items that are already present in this.rest will be sorted out
   * @param provider
   */
  private async getOffStreetData(provider: 'liveparking' | 'bliq') {
    const location = this.mapService.getCameraPosition();
    try {
      if (provider === 'liveparking') {
        return await this.restService.getOnStreetParkingBasic(location.lat, location.lng);
      } else if (provider === 'bliq') {
        return !(await this.settings.trafficEnabled()) ? await this.restService.getOffStreetParking(location.lat, location.lng) : [];
      }
    } catch (e) {
      console.error('Couldn\'t load off street parking', e);
      return [];
    }
  }

  /** Used in User Interface of this Page */
  async showUserLocationOnMap() {
    try {
      // Reset originLocation, so distance will be calculated by users position and the parking option
      // Otherwise the location will be calculated by the position of the last search result and the parking option
      this.originLocation = null;
      await this.mapService.showUserLocation();
    } catch {
      const alert = await this.alertController.create({
        header: 'Ortungsdienste aktivieren',
        message: 'Bitte aktiviere die Ortungsdienste in den Einstellungen Deines Gerätes.',
        buttons: ['OK']
      });
      await alert.present()
      console.log('Couldn\'t locate user. Location services disabled.')
    }
  }

  async openCityList() {
    await this.navCtrl.navigateForward('citylist', {
      animation: slideTransitionSimple
    });
  }

  /**
   * MODALS / PAGES
   */

  /** Show Locations in a List View */
  async showListViewPage() {
    const navigationExtras = {
      state: {
        parkingOptions: this.rest
      },
      animation: mdTransitionAnimation
    };
    await this.navCtrl.navigateForward('/list-view', navigationExtras);
  }

  async openDetails(item: object) {
    const navigationExtras = {
      state: {
        details: item,
      },
      animation: slideTransitionSimple
    };
    await this.navCtrl.navigateForward('/details', navigationExtras);
  }

  async openPurchasePage() {
    await this.navCtrl.navigateForward('/buy-pro', {
      animation: slideTransitionSimple
    });
  }

  // --- HELPERS ---

  showLoading(value: boolean) {
    this.ngZone.run(() => {
      this.loadingActive = value;
    });
  }

  showResultsNotice(value: boolean) {
    this.ngZone.run(() => {
      this.noResults = value;
    });
  }

  showZoomNotice(value: boolean) {
    this.ngZone.run(() => {
      this.zoomNotice = value;
    });
  }

  /**
   * This function will be executed if user enables traffic on MorePage and comes back to HomePage.
   * The function checks if it's necessary to clear already loaded on street markings from the map.
   * Short: If on-street parking is present, re-init Bliq data, but only with off-street parking.
   */
  private async removeOnStreetMarkings() {
    if (await this.settings.isProUser()) {
      const trafficEnabled = await this.settings.trafficEnabled();
      const itemsContainOnStreet = this.rest.some(item => item.type === 'On-Street');
      if (trafficEnabled && itemsContainOnStreet) {
        this.rest = this.rest.filter(item => item.type === 'Off-Street');
        await this.mapService.clearPolylines();
      }
    }
  }

  /**
   * Simple loop through all items to calculate the distance
   * If no location param added, distance will be calculated by user position
   * If location param added, distance will be calculated by given position
   */
  private async calcDistance(items: ParkingOptions[], customLocation?: ILatLng) {
    await Promise.all(items.map(async item => {
      const distanceByUserLocation = await this.mapService.calcDistance(item.location);
      item.distance = distanceByUserLocation;
      // If user clicks on a search result, distanceByLocation is the distance between the search result and the parking option
      // If user clicks on "locate me", distanceByLocation is the distance between the users position and the parking option
      item.distanceByLocation = customLocation ? await this.mapService.calcDistanceBetweenPoints(item.location, customLocation) : distanceByUserLocation;
    }));
    return items;
  }

  /**
   * Recalculate distance of every item continually when car is moving
   */
  private startDistanceRecalculationInterval() {
    this.mapService.watchPosition(10).subscribe(async coords => {
      console.log('Recalculating Distance...');
      if (this.rest?.length && !this.originLocation) {
        await Promise.all(this.rest.map(async item => {
          item.distance = await this.mapService.calcDistance(coords);
        }));
      }
    })
  }

  /** Set right marker and replace info when slide changes */
  async syncSlideInfo() {
    const index = await this.slides.getActiveIndex();
    this.activeSlideIndex = index; // Used in HTML to hide items to improve performance
    // Prevent error if rest is is empty
    if (this.rest.length) {
      await this.mapService.selectMarker(this.rest[index].location);
    }
  }

  /** Selects Slide by Location/Facility ID on Marker Click */
  private async selectSlidebyLocationId(markerId: number | string) {
    const currentActiveSlide = await this.slides.getActiveIndex();
    const index = this.rest.findIndex(item => item.id === markerId);
    this.activeSlideIndex = index; // Used in HTML to hide items to improve performance

    if (currentActiveSlide === index) {
      // Open details if item is selected and user clicks on marker
      this.ngZone.run(async () => await this.openDetails(this.rest[index]));
    } else {
      // If item not selected, select on marker click
      await this.slides.slideTo(index);
    }
  }

  /** Check if user still owns a valid subscription (will be executed automatically on page visit) */
  private async checkUserSubscriptionStatus() {
    await this.platform.ready();
    const isProUser = await this.settings.isProUser();
    // inly needs to be executed if user is pro
    if (isProUser && this.platform.is('cordova')) {
      const userHasActiveSubscription = await this.purchaseService.checkSubscriptionStatus();
      console.log('userHasActiveSubscription', userHasActiveSubscription);
      if (!userHasActiveSubscription) {
        this.rest = [];
        this.isProUser = false;
        await this.settings.removeProStatus();
        await this.mapService.clearMap();
        await this.showProStatusNotice();
        await this.loadData(); // load basic data
      }
    }
  }

  private async showProStatusNotice() {
    const alert = await this.alertController.create({
      header: 'PRO-Status abgelaufen',
      message: 'Deine PRO-Mitgliedschaft ist leider abgelaufen. Du wurdest auf Basic zurückgestuft.',
      buttons: [
        {
          text: 'Verlängern',
          handler: async () => {
            await this.navCtrl.navigateForward('/buy-pro', {
              animation: slideTransitionSimple
            });
          }
        }, {
          text: 'Schließen',
          role: 'cancel'
        }
      ]
    });
    await alert.present();
  }

  // --- EVENTS ---

  private subscribeToQueryParams() {
    this.route.queryParams.subscribe(async () => {
      // User chooses city from CityListPage
      if (this.router.getCurrentNavigation()?.extras?.state?.city) {
        const location = this.router.getCurrentNavigation().extras.state.city.location;
        this.originLocation = location;
        this.rest = [];
        await this.mapService.clearMap();
        await this.mapService.markPosition('searchResult', location);
        await this.mapService.animateCamera(location, 18, 500);
      }
      // Check if user upgrades to PRO
      if (this.router.getCurrentNavigation()?.extras?.state?.proUpgrade) {
        this.isProUser = true;
        await this.loadData();
      }
    });
  }

  private subscribeToCustomEvents() {
    // Disable hardware back button (android)
    this.platform.backButton.subscribe(() => { });

    this.settings.trafficEnabledListener().subscribe(async enabled => {
      if (this.isProUser) {
        if (enabled) {
          await this.removeOnStreetMarkings();
        } else {
          await this.getOnStreetData();
        }
      }
    });

    this.mapService.markerClicked().subscribe(async item => {
      if (item) {
        await this.selectSlidebyLocationId(item.id);
      }
    });
  }
}
