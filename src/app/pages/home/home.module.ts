import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { HomePage } from './home.page';
import { SearchbarComponent } from '../../components/searchbar/searchbar.component';
// Native
import { Purchases } from '@ionic-native/purchases/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@NgModule({
  imports: [
    FilterPipeModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [
    HomePage,
    SearchbarComponent
  ],
  providers: [
    Purchases,
    Keyboard,
    SearchbarComponent
  ]
})
export class HomePageModule { }
