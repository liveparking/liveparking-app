import { Component, OnInit } from '@angular/core';
import { Platform, ToastController, NavController } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OrderPipe } from 'ngx-order-pipe';
import { MapService } from '../../services/map.service';
import { SettingsService } from '../../services/settings.service';
import { slideTransitionSimple } from '../../transitions/page-transition';
import { Dialogs } from '@ionic-native/dialogs/ngx';

@Component({
  selector: 'app-citylist',
  templateUrl: './citylist.page.html',
  styleUrls: ['./citylist.page.scss'],
})
export class CitylistPage implements OnInit {

  cityList = [];
  selectedCitySlug: string;
  // City request
  requestedCity = '';
  showLoadingSpinnerInButton = false;
  buttonText = 'Weiter';

  constructor(
    private statusBar: StatusBar,
    private rest: RestService,
    private platform: Platform,
    private mapService: MapService,
    private settingsService: SettingsService,
    private toastController: ToastController,
    private orderPipe: OrderPipe,
    private navCtrl: NavController,
    private dialogs: Dialogs) { }

  async ngOnInit() {
    await this.loadCityList();
  }

  async loadCityList(event = null) {
    try {
      let cities: any = await this.rest.getCityIndex();
      try {
        cities = await this.calcDistance(cities);
        cities = this.orderPipe.transform(cities, 'distance');
      } catch (e) {
        // fallback if location services are disabled
        cities = this.orderPipe.transform(cities, 'name');
      }
      this.cityList = this.convertArray(cities);
      // await this.preselectItem();
    } catch {
      const toast = await this.toastController.create({
        message: 'Fehler beim Laden der Städte.',
        duration: 2000,
        cssClass: 'custom-toast'
      });
      toast.present();
    }

    if (event) {
      event.target.complete();
    }
  }

  async calcDistance(cityList: any[]) {
    for (const city of cityList) {
      city.distance = await this.mapService.calcDistance(city.location);
    }
    return cityList;
  }

  convertArray(cityList: any[]) {
    const orderedCityList = [];
    for (const city of cityList) {
      if (city?.status === 'active') {
        const found = orderedCityList.some(item => item.country === city.country);
        if (!found) {
          orderedCityList.push({
            country: city.country,
            cities: []
          })
        }
        const index = orderedCityList.findIndex(item => item.country === city.country);
        orderedCityList[index].cities.push(city);
      }
    }
    return orderedCityList;
  }

  async preselectItem() {
    const city = await this.settingsService.getCity();
    if (city) {
      this.selectedCitySlug = city.slug; // Pre-Select List Item
    }
  }

  async setCity(city: any) {
    // Store new information
    await this.settingsService.setCity(city);
    // Tell HomePage to change city
    await this.navCtrl.navigateBack('/', {
      state: { city },
      animation: slideTransitionSimple
    });
  }

  async showInfoDialog() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.dialogs.alert('Partnerstädte stellen uns die Daten ihres Parkleitsystems kostenlos zur Verfügung. Diese Daten geben wir ebenso kostenlos an die Anwender:innen weiter. Wir arbeiten mit Hochdruck daran, weitere Städte zu ergänzen.\n\nIn der PRO-Version hingegen werden die Daten rechnerisch über eine künstliche Intelligenz bestimmt. Aus diesem Grund steht in der PRO-Version eine größere Anzahl an Städten zur Verfügung.', 'Information', 'Schließen');
    }
  }

  async close() {
    await this.navCtrl.navigateBack('/tabs/more');
  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.statusBar.styleDefault();
      }
    });
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.statusBar.styleLightContent();
      }
    });
  }
}
