import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OrderModule } from 'ngx-order-pipe';
import { CitylistPage } from './citylist.page';
import { ComponentsModule } from '../../components/components.module';
import { CityRequestComponent } from '../../components/city-request/city-request.component';
import { Keyboard } from '@ionic-native/keyboard/ngx'; // Used in CityRequestComponent
import { Dialogs } from '@ionic-native/dialogs/ngx';

const routes: Routes = [
  {
    path: '',
    component: CitylistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CitylistPage, CityRequestComponent],
  providers: [
    Keyboard,
    Dialogs,
    CityRequestComponent
  ]
})
export class CitylistPageModule {}
