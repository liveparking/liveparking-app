import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { MapService } from '../../services/map.service';
import { Platform, ActionSheetController, AlertController, NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
// Native
import { AppVersion } from '@ionic-native/app-version/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-more',
  templateUrl: './more.page.html',
  styleUrls: ['./more.page.scss'],
})
export class MorePage implements OnInit {

  appInfo: any = {
    version: 0,
    code: 0
  };
  mapStyleIsSatellite: boolean;
  mapShowTraffic: boolean;
  mapShowOnStreetParking: boolean;
  mapShowOffStreetParking: boolean;
  isProUser = false;
  showDevTools = false;

  constructor(
    private platform: Platform,
    private settings: SettingsService,
    private mapService: MapService,
    private actionSheetController: ActionSheetController,
    private safariViewController: SafariViewController,
    private appVersion: AppVersion,
    private socialSharing: SocialSharing,
    private navController: NavController,
    private alertController: AlertController,
    private inAppBrowser: InAppBrowser) { }

  async ionViewWillEnter() {
    this.isProUser = await this.settings.isProUser();
    await this.applySettings();
  }

  async ngOnInit() {
    // Set Toggle for Satellite
    const mapStyle = await this.mapService.getMapType();
    this.mapStyleIsSatellite = (mapStyle === 'MAP_TYPE_SATELLITE') ? true : false;
    // Get App Version to show at Bottom of this Page
    await this.getAppVersion();
    this.showDevTools = !environment.production;
  }

  async changeMapStyle() {
    if (this.mapStyleIsSatellite) {
      await this.mapService.setMapType('MAP_TYPE_SATELLITE');
    } else if (!this.mapStyleIsSatellite) {
      await this.mapService.setMapType('MAP_TYPE_NORMAL');
    }
  }

  async enableMapTraffic(active: boolean) {
    this.mapShowTraffic = active
    await this.mapService.setTrafficEnabled(active);
    await this.settings.setTrafficEnabled(active);
    // Disable On Street
    if (this.isProUser) {
      this.mapShowOnStreetParking = !active;
      await this.settings.setOnStreetParkingEnabled(!active);
    }
  }

  async enableOnStreetParking(active: boolean) {
    this.mapShowOnStreetParking = active;
    await this.settings.setOnStreetParkingEnabled(active);
    // Deactivate Map Traffic
    if (active && this.mapShowTraffic) {
      this.mapShowTraffic = false;
      await this.mapService.setTrafficEnabled(false);
    }
  }

  async applySettings() {
    this.mapShowOffStreetParking = true;
    this.mapShowTraffic = await this.settings.trafficEnabled(); // Traffic Setting
    (this.isProUser && !this.mapShowTraffic) ? await this.enableOnStreetParking(true) : await this.enableOnStreetParking(false); // On-Street Setting

    const mapStyle = await this.mapService.getMapType();
    this.mapStyleIsSatellite = (mapStyle === 'MAP_TYPE_SATELLITE') ? true : false;
  }

  // async setOffStreetParkingSetting() {
  //   if (this.mapShowOffStreetParking) {
  //     await this.settings.setOffStreetParkingEnabled(true);
  //   } else {
  //     await this.settings.setOffStreetParkingEnabled(false);
  //   }
  // }

  async openBrowser(link: string) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.safariViewController.show({
        url: link,
        tintColor: '#4F536A'
      }).toPromise();
    }
  }

  async shareApp() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.socialSharing.share('Lade dir jetzt die LiveParking App kostenlos auf dein Handy und finde freie Parkplätze in Echtzeit!\nhttps://liveparking.eu/app/');
    }
  }

  async deleteCache() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Alle Einstellungen und zwischengespeicherten Daten gehen dabei verloren. Dein Abo geht dabei nicht verloren. Du musst lediglich einmal auf "Käufe wiederherstellen" klicken.',
      buttons: [{
        text: 'Cache leeren',
        role: 'destructive',
        handler: async () => {
          await this.settings.reset();
        }
      }, {
        text: 'Abbrechen',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async parkingPosOptions() {

  }

  async upsellElem() {
    if (!this.isProUser) {
      await this.navController.navigateForward('/buy-pro');
    }
  }

  writeEmail() {
    this.platform.ready().then(() => {
      this.inAppBrowser.create(`mailto:${environment.supportEmail}`, '_system');
    });
  }

  async proUserActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'PRO-Vorteile ansehen',
        handler: async () => {
          await this.navController.navigateForward('/buy-pro');
        }
      }, {
        text: 'Kündigen',
        role: 'destructive',
        handler: () => {
          setTimeout(async () => {
            await this.subscriptionCancellationInfo();
          }, 250);
        }
      }, {
        text: 'Schließen',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }

  async subscriptionCancellationInfo() {
    const alert = await this.alertController.create({
      header: 'So funktionert\'s',
      message: 'Bei iOS kannst Du die Kündigung über die Einstellungen Deines Gerätes vornehmen und bei Android über den Play Store. In beiden Fällen findest Du eine Anleitung des Herstellers im Internet.',
      buttons: ['OK']
    });

    await alert.present();
  }

  /**
   * Get current app version to display on the bottom of the page
   */
  async getAppVersion() {
    try {
      await this.platform.ready();
      if (this.platform.is('cordova')) {
        this.appInfo.version = await this.appVersion.getVersionNumber();
        this.appInfo.code = await this.appVersion.getVersionCode();
      }
    } catch (e) {
      console.error(e);
    }
  }

  async clearStorage() {
    await this.settings.clearStorage();
    alert('App bitte neustarten!')
  }

}
