import { Component, OnInit } from '@angular/core';
import { Platform, NavController, ToastController, ModalController, IonRouterOutlet } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { SettingsService } from '../../services/settings.service';
import { LaunchReview } from '@ionic-native/launch-review/ngx';
import { RatingModalComponent } from '../../components/rating-modal/rating-modal.component';

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.page.html',
  styleUrls: ['./feedback-form.page.scss'],
})
export class FeedbackFormPage implements OnInit {

  showLoading = false;
  rating: number;
  sender = {
    name: '',
    email: '',
    message: ''
  };

  constructor(
    private platform: Platform,
    private navController: NavController,
    private restService: RestService,
    private settings: SettingsService,
    private routerOutlet: IonRouterOutlet,
    private toastController: ToastController,
    private modalController: ModalController,
    private launchReview: LaunchReview) { }

  async ngOnInit() {
    this.sender.email = await this.settings.getUserEmail() || '';
    this.sender.name = await this.settings.getUserName() || '';
  }

  segmentChanged(e: CustomEvent) {
    this.rating = Number(e.detail.value);
  }

  private validateEmail() {
    const regexp = new RegExp(/^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{1,10}$/);
    return regexp.test(this.sender.email);
  }

  async presentToast(text: string) {
    const toast = await this.toastController.create({
      message: text,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }

  async showRatingModal() {
    const ratingModal = await this.modalController.create({
      component: RatingModalComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    ratingModal.onWillDismiss()
      .then(async () => {
        await this.navController.navigateBack('/tabs/more');
      });
    return await ratingModal.present();
  }

  async submitFeedback() {

    // Uncomment if rating is necessary
    // if (!this.rating) {
    //   await this.presentToast('Bitte wähle einen Emoji aus.');
    //   return;
    // }

    if (!this.validateEmail()) {
      await this.presentToast('Bitte gültige E-Mail Adresse angeben.');
      return;
    }

    this.showLoading = true;

    try {
      await this.settings.setUserEmail(this.sender.email);
      await this.settings.setUserName(this.sender.name);
      await this.restService.sendFeedback(this.sender);
    } catch {
      console.log('Error sending mail.');
    } finally {
      this.showLoading = false;
    }

    if (this.rating >= 4) {
      if (this.platform.is('cordova')) {
        if (this.launchReview.isRatingSupported()) {
          // User rated 4 stars, rating is supported
          await this.launchReview.rating().toPromise();
          this.navController.back();
        } else {
          // User rated 4 stars, rating not supported
          await this.showRatingModal();
        }
      } else {
        // User rated 4 stars, cordova not available (browser/pwa)
        await this.showRatingModal();
      }
    } else {
      // User rated below 4 stars
      this.navController.back();
    }
  }

}
