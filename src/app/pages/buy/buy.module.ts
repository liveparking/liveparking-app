import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { BuyPageRoutingModule } from './buy-routing.module';
import { BuyPage } from './buy.page';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { LottieModule } from 'ngx-lottie';

@NgModule({
  imports: [
    LottieModule,
    CommonModule,
    FormsModule,
    IonicModule,
    BuyPageRoutingModule
  ],
  providers: [
    SafariViewController,
    Dialogs
  ],
  declarations: [BuyPage]
})
export class BuyPageModule {}
