import { Component, OnInit } from '@angular/core';
import { Platform, AnimationController, NavController } from '@ionic/angular';
import { SettingsService } from '../../services/settings.service';
import { RestService } from '../../services/rest.service';
import { MapService } from '../../services/map.service';
import { PurchaseService } from '../../services/purchase.service';
import { PurchasesPackage, PACKAGE_TYPE, PurchasesOfferings } from '@ionic-native/purchases/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { slideTransitionSimple } from '../../transitions/page-transition';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.page.html',
  styleUrls: ['./buy.page.scss'],
})
export class BuyPage implements OnInit {

  features: object;
  offerings: PurchasesOfferings;
  showLoadingIndicator = false;
  isProUser: boolean;
  selectedPriceIndex = 0;

  constructor(
    private platform: Platform,
    private purchase: PurchaseService,
    private settings: SettingsService,
    private restService: RestService,
    private mapService: MapService,
    private navController: NavController,
    private animationCtrl: AnimationController,
    private safariViewController: SafariViewController,
    private dialogs: Dialogs) { }

  async ngOnInit() {
    console.log(history);
    this.features = [
      {
        name: '500+ Städte',
        description: 'In über 500 deutschen Städten jederzeit einen freien Parkplatz finden!',
        iconPath: '/assets/buy-pro/buildings.svg'
      },
      {
        name: 'Parkstreifen (On-Street)',
        description: 'Auch freie Parkplätze auf dem Seitenstreifen werden angezeigt.',
        iconPath: '/assets/buy-pro/on-street.svg'
      },
      {
        name: 'Zusätzliche Inhalte',
        description: 'Preise, Öffnungszeiten und viele weitere Infos zu jeder Parkmöglichkeit.',
        iconPath: '/assets/buy-pro/additional-info.svg'
      },
      {
        name: 'Künstliche Intelligenz',
        description: 'Die KI berechnet freie Parkplätze im Voraus. Sei der Erste vor Ort!',
        iconPath: '/assets/buy-pro/prediction.svg'
      },
      // {
      //   name: 'Erweiterte Suche',
      //   description: 'Suche nach Orten und Locations und schaue nach wie viel am Ziel frei ist.',
      //   iconPath: '/assets/buy-pro/extended-search.svg'
      // },
      {
        name: 'Werbefrei',
        description: 'Nutze die App Werbefrei. Alle Werbeanzeigen werden entfernt.',
        iconPath: '/assets/buy-pro/advertising.svg'
      },
      {
        name: 'Einfache Kündigung',
        description: 'Nur einen Monat Vertragslaufzeit. 24h vor Verlängerung kündbar.',
        iconPath: '/assets/buy-pro/agreement.svg'
      }
    ];

    try {
      this.isProUser = await this.settings.isProUser();
      console.log('this.isProUser', this.isProUser);
      await this.getOfferings();
      await this.startButtonAnimation();
    } catch (e) {
      console.error(e);
    }
  }

  async getOfferings() {
    this.showLoadingIndicator = true;
    this.offerings = await this.purchase.getOfferings();
    this.showLoadingIndicator = false;
  }

  async buyProduct() {
    if (!this.isProUser && !this.showLoadingIndicator && this.offerings) {
      this.showLoadingIndicator = true;
      const result = await this.purchase.purchaseProduct(this.offerings.current.monthly);
      this.showLoadingIndicator = false;
      if (result?.success) {
        this.setUserAsPro();
      }
    }
  }

  async restorePurchase() {
    const result = await this.purchase.restorePurchase();
    if (result?.success) {
      this.setUserAsPro();
    }
  }

  
async getPurchaserInfo() {
  const resp = await this.purchase.getPurchaserInfo();
  console.log(resp);
}

  async openBrowser(link: string) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.safariViewController.show({
        url: link,
        tintColor: '#4F536A'
      }).toPromise();
    }
  }

  private async setUserAsPro() {
    this.isProUser = true;
    await this.settings.setUserAsPro(true);
    await this.settings.setOnStreetParkingEnabled(true);
    await this.navController.navigateBack('/tabs/home', {
      state: { proUpgrade: true },
      animation: slideTransitionSimple
    });
  }

  async openCouponAlert() {
    const result = await this.purchase.openCouponAlert();
    if (result?.success) {
      await this.setUserAsPro();
    }
  }

  async showInfoDialog() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
     const buttonIndex = await this.dialogs.confirm('In der PRO-Version werden freie Parkplätze durch Hinzunahme verschiedener Parameter rechnerisch über eine künstliche Intelligenz bestimmt. Aus diesem Grund steht in der PRO-Version eine größere Anzahl an Städten zur Verfügung. Zusätzlich können freie Parkplätze außerhalb auf Parkstreifen bestimmt werden.\n\nDie kostenlose Version bezieht sich auf die Parkleitsysteme ausgewählter Partnerstädte und stellt freie Parkplätze in Parkhäusern, Tiefgaragen und Co. dar. ', 'PRO vs. Free', ['Partnerstädte', 'Schließen']);
     if (buttonIndex === 1) {
       this.navController.navigateForward('/citylist');
     }
    }
  }

  private async cityNotAvailable(city: string, nextAvailableCity: string) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const buttonIndex = await this.dialogs.confirm(`Tut uns leid! Es sieht so aus, als wäre die Stadt ${city ? city : 'in der Du Dich befindest'} noch nicht in LiveParking PRO verfügbar. ${nextAvailableCity ? '\n\nNächste verfügbare Stadt: ' + nextAvailableCity : ''}`, 'Stadt nicht verfügbar', ['Andere Stadt testen', 'Schließen']);
      if (buttonIndex === 1) {
        
      }
    }
  }
  
  async checkAvailability() {
    this.showLoadingIndicator = true;
    const position = await this.mapService.getUserLatLng();
    const address = await this.mapService.latLngToAddress(position)
    const response = await this.restService.getOnStreetParking(position.lat, position.lng);
    if (response.length) {
      const distance = await this.mapService.calcDistance(response[0].location);
      if (distance > 2000) {
        this.showLoadingIndicator = false;
        const nextAvailableCity = await this.mapService.latLngToAddress(response[0].location);
        await this.cityNotAvailable(address[0]?.locality, nextAvailableCity[0]?.locality);
      } else {
        await this.buyProduct();
      }
    } else {
      const nextAvailableCity = await this.mapService.latLngToAddress(response[0].location);
      this.showLoadingIndicator = false;
     await this.cityNotAvailable(address[0]?.locality, nextAvailableCity[0]?.locality);
    }
  }

  calcDiscountInPercent(offering: PurchasesPackage) {
    const cheapestPrice = this.offerings.current.annual.product.price;
    let duration: any;
    switch (offering.packageType) {
      case PACKAGE_TYPE.MONTHLY:
        duration = 1;
        break;
      case PACKAGE_TYPE.THREE_MONTH:
        duration = 3;
        break;
      case PACKAGE_TYPE.SIX_MONTH:
        duration = 6;
        break;
      case PACKAGE_TYPE.ANNUAL:
        duration = 12;
        break;
      default:
    }
    const normalPrice = cheapestPrice * duration;
    const discountedPrice = offering.product.price;
    return ((discountedPrice / normalPrice) - 1) * -100;
  }

  priceWithoutDiscount(offering: PurchasesPackage) {
    const cheapestPrice = this.offerings.current.annual.product.price;
    let duration: any;
    switch (offering.packageType) {
      case PACKAGE_TYPE.MONTHLY:
        duration = 1;
        break;
      case PACKAGE_TYPE.THREE_MONTH:
        duration = 3;
        break;
      case PACKAGE_TYPE.SIX_MONTH:
        duration = 6;
        break;
      case PACKAGE_TYPE.ANNUAL:
        duration = 12;
        break;
      default:
    }
    const normalPrice = Math.ceil(cheapestPrice * duration); // Round up

    if (offering.product.currency_code === 'EUR') {
      return String((normalPrice - 0.01) + ' €').replace('.', ','); // Example: 19,99€
    } else {
      return String(offering.product.currency_code + ' ' + (normalPrice - 0.01)).replace('.', ','); // Example: CHF 19,99
    }
  }

  private async startButtonAnimation() {
    await this.animationCtrl.create()
      .addElement(document.querySelector('.buy-button'))
      .duration(5000)
      .iterations(Infinity)
      .keyframes([
        { offset: 0, transform: 'scale(1)' },
        { offset: 0.09, transform: 'scale(1)' },
        { offset: 0.12, transform: 'scale(1.05)' },
        { offset: 0.16, transform: 'scale(0.95)' },
        { offset: 0.20, transform: 'scale(1.03)' },
        { offset: 0.24, transform: 'scale(1)' },
        { offset: 1, transform: 'scale(1)' }
      ]).play();
  }
}
