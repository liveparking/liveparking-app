import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ListViewPageRoutingModule } from './list-view-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { ListViewPage } from './list-view.page';
// Native
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';

@NgModule({
  imports: [
    FilterPipeModule,
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ListViewPageRoutingModule
  ],
  providers: [StatusBar, SafariViewController],
  declarations: [ListViewPage]
})
export class ListViewPageModule {}
