import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, IonSearchbar, Platform } from '@ionic/angular';
import { SettingsService } from '../../services/settings.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.page.html',
  styleUrls: ['./list-view.page.scss'],
})
export class ListViewPage implements OnInit, AfterViewInit {

  spots = [];
  isProUser: boolean;
  searchTerm = { name: '' };
  results = [];
  filter = 'All';
  autoFocus = false;
  maxResults = 10;

  @ViewChild('searchbar', { static: false }) searchbar: IonSearchbar;

  constructor(
    private navCtrl: NavController,
    private platform: Platform,
    private statusBar: StatusBar,
    private route: ActivatedRoute,
    private router: Router,
    private settings: SettingsService,
    private safariViewController: SafariViewController) { }

  async ngOnInit() {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation()?.extras?.state?.parkingOptions) {
        this.spots = this.router.getCurrentNavigation()?.extras?.state?.parkingOptions;
        this.results = this.spots;
      }
      const showKeyboard = this.router.getCurrentNavigation()?.extras?.state?.showKeyboard;
      if (showKeyboard) {
        setTimeout(async () => {
          await this.searchbar.setFocus();
        }, 200);
      }
    });
    this.isProUser = await this.settings.isProUser();
  }

  async ngAfterViewInit() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      setTimeout(() => {
        this.statusBar.styleDefault();
      }, 300);
    }
  }

  async ionViewWillLeave() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      setTimeout(() => {
        this.statusBar.styleLightContent();
      }, 100);
    }
  }

  async openDetails(item: object) {
    await this.navCtrl.navigateForward('/details', {
      state: {
        details: item,
      }
    });
  }

  filterItems(e: CustomEvent) {
    if (e.detail.value === 'All') {
      this.results = this.spots;
    } else {
      this.results = this.spots.filter(spot => {
        return spot.type === e.detail.value;
      });
    }
  }

  resetFilter() {
    this.filter = 'All';
    this.maxResults = 10;
  }

  search(e: CustomEvent) {
    this.results = this.spots.filter(spot => {
      if (spot?.name) {
        return spot.name.toLowerCase().includes(e.detail.value.toLowerCase());
      }
    });
  }

  resetSearch() {
    this.results = this.spots;
    this.maxResults = 10;
  }

  displayMoreItems(event: any) {
    // Max 60 items due to performance issues
    if (this.maxResults < 60) {
      this.maxResults = this.maxResults + 10;
    }
    event.target.complete();
  }

  async close() {
    if (this.platform.is('cordova')) {
      this.statusBar.styleLightContent();
    }
    await this.navCtrl.navigateBack('/tabs/home');
  }

  async openCityList() {
    if (this.platform.is('cordova')) {
      this.statusBar.styleLightContent();
    }
    await this.navCtrl.navigateForward('citylist');
  }

  async openPartnerWebsite() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.safariViewController.show({
        url: 'https://bliq.ai',
        tintColor: '#4F536A'
      }).toPromise();
    }
  }
}
