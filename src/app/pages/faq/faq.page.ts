import { Component, OnInit } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { RestService } from '../../services/rest.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx'
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {

  entries = [];

  constructor(
    private platform: Platform,
    private restService: RestService,
    private navController: NavController,
    private inAppBrowser: InAppBrowser) { }

  async ngOnInit() {
    try {
      this.entries = await this.restService.getFaq();
    } catch {
      this.entries = [
        {
          title: 'Fehler beim Laden der Einträge',
          content: 'Bitte versuche es später erneut'
        }
      ];
    }
  }

  writeEmail() {
    this.platform.ready().then(() => {
      this.inAppBrowser.create(`mailto:${environment.supportEmail}`, '_system');
    });
  }

  async navigateTo(path: string) {
    await this.navController.navigateForward(path);
  }

}
