import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OrderModule } from 'ngx-order-pipe';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { MomentModule } from 'ngx-moment';
import { ChartModule } from 'angular-highcharts';

// Components
import { PublicTransportComponent } from '../../components/modals/public-transport/public-transport.component';
import { ChargingStationsComponent } from '../../components/modals/charging-stations/charging-stations.component';
import { ChartComponent } from '../../components/chart/chart.component';

// Native
import { Spherical } from '@ionic-native/google-maps/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { DetailsPage } from './details.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderModule,
    FilterPipeModule,
    MomentModule,
    ChartModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DetailsPage,
    PublicTransportComponent,
    ChargingStationsComponent,
    ChartComponent
  ],
  providers: [
    Spherical,
    LaunchNavigator,
    SafariViewController,
    Dialogs,
    InAppBrowser
  ],
  entryComponents: [
    PublicTransportComponent,
    ChargingStationsComponent
  ]
})
export class DetailsPageModule {}
