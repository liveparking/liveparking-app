import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ToastController, ModalController, IonRouterOutlet, ActionSheetController, NavController } from '@ionic/angular';
// Native
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// Services
import { RestService, ParkingOptions } from '../../services/rest.service';
import { MapService, ILatLng } from '../../services/map.service';
import { SettingsService } from '../../services/settings.service';
// Components
import { PublicTransportComponent } from '../../components/modals/public-transport/public-transport.component';
import { ChargingStationsComponent } from '../../components/modals/charging-stations/charging-stations.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  bliqData: any = null;
  inrixData: any = null;
  details: ParkingOptions;
  isProUser = false;
  // Chart
  chartValue = null;
  chartSuffix = '';
  chartText = '';
  chartMaxValue = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private routerOutlet: IonRouterOutlet,
    private dialogs: Dialogs,
    private toastController: ToastController,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private navController: NavController,
    private platform: Platform,
    private restService: RestService,
    private settingsService: SettingsService,
    private launchNavigator: LaunchNavigator,
    private safariViewController: SafariViewController,
    private mapService: MapService,
    private inAppBrowser: InAppBrowser) { }

  async ngOnInit() {
    this.mapService.setBackgroundToBlack();

    // Nav Params
    this.route.queryParams.subscribe(async () => {
      if (this.router.getCurrentNavigation().extras.hasOwnProperty('state')) {
        if (this.router.getCurrentNavigation().extras.state.hasOwnProperty('details')) {
          this.details = this.router.getCurrentNavigation().extras.state.details;
        }
      }
    });

    // Chart Info
    if (this.isNumber(this.details.freeSpots)) {
      this.chartValue = this.details.freeSpots;
      this.chartText = 'Freie Plätze';
      this.chartMaxValue = this.details.allSpots;
    } else if (this.isNumber(this.details.parkingProbability)) {
      this.chartValue = this.details.parkingProbability;
      this.chartSuffix = '%';
      this.chartText = 'Wahrscheinlichkeit';
      this.chartMaxValue = 100;
    } else {
      this.chartValue = '?';
      this.chartText = 'Keine Angabe';
      this.chartMaxValue = 0;
    }

    this.isProUser = await this.settingsService.isProUser();
  }

  // Pro Feature: INRIX
  async loadInrixData() {
    let resp: any;
    if (this.bliqData.geometry.coordinates.length === 2) {
      resp = await this.restService.getInrixOnStreetDataByBox(this.bliqData.geometry.coordinates);
    } else {
      resp = await this.restService.getInrixOnStreetDataByPoint(this.bliqData.properties.parkingEntity.centerPoint.coordinates[1], this.bliqData.properties.parkingEntity.centerPoint.coordinates[0]);
    }
    this.inrixData = resp.result[0];
    console.log(this.inrixData);
  }

  async refreshDetails(event?: any) {
    try {
      const response = await this.restService.getFacility(this.details.id);
      this.details = response[0];
      if (event) {
        event.target.complete();
      }
    } catch {
      const toast = await this.toastController.create({
        message: 'Fehler beim Aktualisieren.',
        duration: 2000,
        cssClass: 'custom-toast'
      });
      toast.present();
    }
  }

  async showOptions() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Navigation',
        handler: () => {
          // Without timeout the action sheet wouldn't close automatically
          setTimeout(async () => {
            await this.openNavigator(this.details.location);
          }, 1);
        }
      }, {
        text: 'Ladesäulen',
        handler: () => {
          setTimeout(async () => {
            await this.openModal('cs');
          }, 200);
        }
      }, {
        text: 'ÖPNV',
        handler: () => {
          setTimeout(async () => {
            await this.openModal('pt');
          }, 200);
        }
      }, {
        text: 'Abbrechen',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }


  private async openNavigator(location: ILatLng, destination?: string) {
    await this.platform.ready();
    if (this.platform.is('cordova')) {

      const options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: destination ? 'Navigieren zu ' + destination : 'App für Navigation wählen',
          cancelButtonText: 'Abbrechen',
          rememberChoice: {
            enabled: false
          }
        }
      };
      try {
        await this.launchNavigator.navigate([location.lat, location.lng], options);
      } catch (e) {
        console.log(e);
      }
    }
  }


  private async openModal(modal: 'pt' | 'cs') {
    const modalOptions = await this.modalController.create({
      component: modal === 'pt' && PublicTransportComponent || modal === 'cs' && ChargingStationsComponent,
      componentProps: {
        location: this.details.location
      },
      swipeToClose: true,
    });
    if (window.innerWidth < 990) {
      modalOptions.presentingElement = this.routerOutlet.nativeEl;
    }
    return await modalOptions.present();
  }

  async displayExtraInfo() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.dialogs.alert(this.details.extras, 'Info', 'OK')
    }
  }

  // Used to connect all words
  // and putting an comma between them
  private arrayToString(array: any[]) {
    if (array) {
      let output = '';
      array.forEach(element => {
        output += element + '<br>';
      });
      output = output.replace(/, \s*$/, '');
      return output;
    } else {
      return null;
    }
  }

  async writeEmail() {
    this.platform.ready().then(() => {
      this.inAppBrowser.create(`mailto:${environment.supportEmail}?subject=Fehlerhafte Daten` , '_system');
    });
  }

  async openPartnerWebsite() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      await this.safariViewController.show({
        url: 'https://bliq.ai',
        tintColor: '#4F536A'
      }).toPromise();
    }
  }

  private isNumber(n: any) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0)
  }

  getInrixData() {

    const features = [];
    if (this.inrixData?.amenities) {
      this.inrixData.amenities.forEach(feature => {
        if (feature.value) {
          features.push(feature.name);
        }
      });
    }
  }

}
